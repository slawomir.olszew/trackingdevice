### Requirements: 
Linux (tested on Ubuntu 20.10)

Anaconda (tested on Anaconda 4.8.3)

CUDA (tested on CUDA V10.0.130)

## Anaconda installation:
Download anaconda installer from [page](https://www.anaconda.com/products/individual#linux), or:

```bash
wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh
```

Add execute permission to installer:

```bash
chmod +x Anaconda3-2020.07-Linux-x86_64.sh
```

Run installer:

```bash
sudo bash Anaconda3-2020.07-Linux-x86_64.sh
```

## Initialization:

Create conda virtual environment and install requirements:

```bash
conda create -n siammask python=3.6
source activate siammask
pip install -r SiamMask/requirements.txt
bash SiamMask/make.sh
```

Download siam-mask model:

```bash
cd SiamMask/experiments/siammask_sharp
wget http://www.robots.ox.ac.uk/~qwang/SiamMask_DAVIS.pth
cd ../../../
```

## Run:

Activate viatual environment and set environment variables:

```bash
source activate siammask
cd SiamMask
export PYTHONPATH=$PWD:$PYTHONPATH
cd experiments/siammask_sharp
```

Run script:

```bash
python siam_mask.py
```

## Arguments:

Path to pretrained model:

```bash
--resume PATH
```

Path to model config file:

```bash
--config PATH
```

Path to video:

```bash
--video PATH
```

Set if want to use live camera:

```bash
--camera
```

Force cpu usage:

```bash
--cpu
```

Handle object out of frame:

```bash
--bounds
```

Save first frame and mask as png:

```bash
--save
```

Path to output save directory (with file prefix):

```bash
--save_path
```

# Instrukcja konfiguracji i uruchomienia SiamMask na systemie Windows
### Wymagane: Python 3.6+, opcjonalnie CUDA (działa na pewno: Python 3.8.0 i CUDA 10.1)
1. Przechodzimy do folderu SiamMask
2. Wywołujemy następującą komendę (odpowiednio do używanego przez nas terminala, w następnych punktach również będzie podział, jeśli będzie to konieczne)
- zwykły terminal 
    ```cmd
    set SiamMask=%cd%
    ```
- PowerShell
    ```powershell
    $env:SiamMask=$pwd
    ```
3. Tworzymy środowisko wirtualne  (dla zwykłego terminala: ```./venv/Scripts/activate.bat```)
    ```cmd
    virtualenv venv
    ./venv/Scripts/activate.ps1
    pip install -r requirements_custom.txt -f https://download.pytorch.org/whl/torch_stable.html
    bash make_windows.sh
    ```
    Powyższy kod zadziała, jeśli mamy basha dodanego do ścieżki. Jeśli nie wykrywa basha, to jak mamy Git Basha to folderze bin jest skrypt ```bash.exe``` (można też wykorzystać inny, spełniający tę funkcję) i za jego pomocą możemy uruchomić ```make_windows.sh``` (zazwyczaj znajduje się on w ```Program Files``` na dysku C, wtedy trzeba zamienić ten fragment ścieżki na ```PROGRA~1```).
    Domyślna ścieżka:
    ```
    C:\PROGRA~1\Git\bin\bash.exe make_windows.sh
    ```
    Alternatywna ścieżka:
    ```
    ścieżka_do_folderu_git\Git\bin\bash.exe make_windows.sh
    ```  
    Zazwyczaj nie działa instalacja przy użyciu komendy ```pip install -r requirements.txt``` (problem z modułem ```torch```), ale można spróbować. Można też ewentualnie zmodyfikować dany moduł na podstawie tego, jaką mamy wersję CUDY (tutaj wersja CUDA - 10.1, jeśli nie mamy, można usunąć/zmienić dopisek ```+cu101``` przy modułach) lub pominąć dopisek ```-f https://download.pytorch.org/whl/torch_stable.html```, ale bez tego raczej nie zadziała
4. Przechodzimy do folderu:
    ```cmd
    cd experiments\siammask_sharp
    ```
5. Wywołujemy komendy do pobrania niezbędnych plików:
- zwykły terminal
    ```cmd
    curl http://www.robots.ox.ac.uk/~qwang/SiamMask_VOT.pth --output SiamMask_VOT.pth
    curl http://www.robots.ox.ac.uk/~qwang/SiamMask_DAVIS.pth --output SiamMask_DAVIS.pth
    ```
- PowerShell
    ```powershell
    wget http://www.robots.ox.ac.uk/~qwang/SiamMask_VOT.pth -outfile SiamMask_VOT.pth
    wget http://www.robots.ox.ac.uk/~qwang/SiamMask_DAVIS.pth -outfile SiamMask_DAVIS.pth
    ```
6. Wywołujemy komendy (można wpisać inaczej jeśli wykrywa nam ścieżkę ```PYTHONPATH```):
- zwykły terminal (ewentualnie można ustawić ```set PYTHONPATH=%SiamMask%;%cd%;%PYTHONPATH%```)
    ```cmd
    set PYTHONPATH=%SiamMask%;%cd%
    ```
- PowerShell (ewentualnie można ustawić ```$env:PYTHONPATH="$env:SiamMask;$pwd;$env:PYTHONPATH"```)
    ```powershell
    $env:PYTHONPATH="$env:SiamMask;$pwd"
    ``` 
7. Uruchamiamy (przykładowa komenda)
    ```cmd
    python siam_mask.py --resume SiamMask_DAVIS.pth --config config_davis.json --camera
    ```
8. Po pomyślnej konfiguracji, przy kolejnych uruchomieniach wystarczą komendy:
- zwykły terminal
    ```cmd
    cd SiamMask
    set SiamMask=%cd%
    ./venv/Scripts/activate.bat
    cd experiments\siammask_sharp
    set PYTHONPATH=%SiamMask%;%cd%
    python siam_mask.py --resume SiamMask_DAVIS.pth --config config_davis.json --camera
    ```
- PowerShell
    ```powershell
    cd SiamMask
    $env:SiamMask=$pwd
    ./venv/Scripts/activate.ps1
    cd experiments\siammask_sharp
    $env:PYTHONPATH="$env:SiamMask;$pwd"
    python siam_mask.py --resume SiamMask_DAVIS.pth --config config_davis.json --camera
    ```
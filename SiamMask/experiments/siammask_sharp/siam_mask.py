"""
### Segmentation and tracking module using [SiamMask - Fast Online Object Tracking and Segmentation](https://github.com/foolwood/SiamMask).
"""
import cv2
from .tools.test import *
from .custom import Custom
import numpy as np
import random


class SiamMask:
    """
    ## SiamMask class traces one object.

    Parameters
    ----------
    siammask
        Initialized siammask instance.
    cfg
        Siammask configuration.
    device
        Torch device.
    mask_colour : list(int) 
        Colour of mask (as [R, G, B] with intensity form 0 to 255).
    bbox_colour : list(int) 
        Colour of  bounding box (as [R, G, B] with intensity form 0 to 255).
    mask_opacity : float
        Mask opacity, 1.0 is full color 0.0 is transparent.
    save_first_frame : bool
        If true saves first processed frame as png.
    
    Attributes
    ----------
    siammask
        Initialized siammask instance.
    cfg
        Siammask configuration.
    device
        Torch device.
    f : int
        Helper variable, if equals 0, process frame as inital, if greater than 0, process frame "normally".
    state : dict
        Contains all the necessary information (state) about Siammask object (mask, masked object location etc.).
    mask_colour : list(int)  
        Colour of mask (as [R, G, B] with intensity form 0 to 255).
    bbox_colour : list(int) 
        Colour of  bounding box (as [R, G, B] with intensity form 0 to 255).
    mask_opacity : float
        Mask opacity, 1.0 is full color 0.0 is transparent.
    save_first_frame : bool
        If true saves first processed frame as png.
    """
    def __init__(self, siammask, cfg, device, mask_colour=[0, 0, 255], bbox_colour=[255, 0, 0], mask_opacity=0.5, save_first_frame=False):
        self.siammask = siammask
        self.cfg = cfg
        self.device = device
        self.f = 0
        self.state = None
        self.save_first_frame = save_first_frame
        self.mask_colour = mask_colour
        self.bbox_colour = bbox_colour
        self.mask_opacity = mask_opacity

    def set_target_pos(self, init_rect):
        """
        Set target rectangle positon and dimensions.

        Parameters
        ----------
        init_rect : tuple
            Initial rectangle boundingbox as Tuple.
        
        Returns
        -------
        None
        """
        x, y, w, h = init_rect
        self.target_pos = np.array([x + w / 2, y + h / 2])
        self.target_sz = np.array([w, h])

    def process(self, frame):
        """
        Main SiamMask process.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        bool
            True if processing was successful.
        frame : numpy.ndarray
            cv2 output frame.
        """
        try:
            if self.f == 0:
                self.state = self.process_first_frame(frame)
            elif self.f > 0:
                self.state, location, mask = self.process_frame(frame)
                frame = self.draw_on_frame(frame, location, mask)
            if not self.state:
                return False, frame
            self.f += 1
            return True, frame
        except Exception as e:
            print("Error:", e)
            return False, frame

    def draw_on_frame(self, frame, location, mask):
        """
        Draw mask and boundingbox on given frame in given location.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        location : numpy.ndarray
            Location of object.
        mask : numpy.ndarray
            Boolean mask of an object, True where object is located (frame size).

        Returns
        -------
        frame : numpy.ndarray
            cv2 output frame.
        """
        org_frame = frame.copy()
        frame[:, :, 0] = (mask > 0) * self.mask_colour[0] + (mask == 0) * frame[:, :, 0]
        frame[:, :, 1] = (mask > 0) * self.mask_colour[1] + (mask == 0) * frame[:, :, 1]
        frame[:, :, 2] = (mask > 0) * self.mask_colour[2] + (mask == 0) * frame[:, :, 2]
        frame = cv2.addWeighted(org_frame, 1 - self.mask_opacity, frame, self.mask_opacity, 0)
        if self.save_first_frame:
            cv2.imwrite(args.save_path + "frame.png", org_frame)
            cv2.imwrite(args.save_path + "mask.png", (mask > 0) * 255 + (mask == 0))
            cv2.imwrite(args.save_path + "frame_mask.png", frame)
        cv2.polylines(frame, [np.int0(location).reshape((-1, 1, 2))], True, (self.bbox_colour[0], self.bbox_colour[1],
                                                                            self.bbox_colour[2]), 3)
        if self.save_first_frame:
            cv2.imwrite(args.save_path + "frame_mask_box.png", frame)
            exit()
        return frame

    def process_first_frame(self, frame):
        """
        Initialize first frame process.

        Parameters
        ----------
        frame : numpy.ndarray
            CV2 frame.
            
        Returns
        -------
        dict
            Contains all the necessary information (state) about Siammask object (mask, masked object location etc.).
        """
        return siamese_init(frame, self.target_pos, self.target_sz, self.siammask, self.cfg['hp'], device=self.device)

    def old_process_frame(self, frame):
        """
        Process siammask tracing on new frame, updates state.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
            
        Returns
        -------
        state : dict
            Contains all the necessary information (state) about Siammask object (mask, masked object location etc.).
        """
        if args.save:
            cv2.imwrite(args.save_path + "frame.png", frame)
            start_tick = cv2.getTickCount()
        state = siamese_track(self.state, frame, mask_enable=True, refine_enable=True, device=self.device)
        location = state['ploygon'].flatten()
        mask = state['mask'] > state['p'].seg_thr
        frame[:, :, 2] = (mask > 0) * 255 + (mask == 0) * frame[:, :, 2]
        if args.save:
            print("First frame processing time: {:02.1f}s".format((cv2.getTickCount() - start_tick) /
                                                                cv2.getTickFrequency()))
            cv2.imwrite(args.save_path + "mask.png", (mask > 0) * 255 + (mask == 0))
            cv2.imwrite(args.save_path + "frame_mask.png", frame)
        cv2.polylines(frame, [np.int0(location).reshape((-1, 1, 2))], True, (0, 255, 0), 3)
        if args.save:
            cv2.imwrite(args.save_path + "frame_mask_box.png", frame)
            exit()
        cv2.imshow('SiamMask', frame)
        return state

    def process_frame(self, frame):
        """
        Process siammask tracing on new frame, updates state.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
            
        Returns
        -------
        state : dict
            Contains all the necessary information (state) about Siammask object (mask, masked object location etc.).
        location : numpy.ndarray
            Location of traced object.
        mask : numpy.ndarray
            Boolean mask of an object, True where object is located (frame size).
        """
        state = siamese_track(self.state, frame, mask_enable=True, refine_enable=True, device=self.device)
        location = state['ploygon'].flatten()
        mask = state['mask'] > state['p'].seg_thr
        return state, location, mask

    def load_model(self):
        """
        Unused, empty function.
        """
        pass

    def get_mask(self):
        """
        Return boolean array with size of frame.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
            
        Returns
        -------
        mask : numpy.ndarray
            Boolean mask of an object, True where object is located (frame size).
        None 
            If mask can't be found.
        """
        if "mask" in self.state and "p" in self.state:
            return self.state['mask'] > self.state['p'].seg_thr
        else:
            return None


class SiamHandling:
    """
    ## Handles Siammask tracing (with multiple objects).

    Attributes
    ----------
    args
        Argparse namespace object.
    path : str
        Path to video file.
    window_name : str
        CV2 window name.
    objects : list
        List of SiamMask objects.
    start : bool
        Allows to start/resume video.
    stop : bool
        Allows to start/resume video.
    """
    def __init__(self, path, args):  
        self.args = args
        if not args.one_frame:
            self.cap = self.video(path) if path else self.camera()
        self.window_name = "SiamMask"
        self.objects = []
        if self.args.camera:
            self.stop = False
        else:
            self.stop = True
        self.start = True

    def toggle_play(self):
        """
        Toggle visualisation play.
        
        Returns
        -------
        None
        """
        self.stop = not self.stop

    def new_select(self):
        """
        Select new ROI.
        
        Returns
        -------
        None
        """
        window_open = self.select_new_ROI(self.frame, self.stop)
        if window_open and self.start:
            self.start = False

    def setup_model(self):
        """
        Setup model and return siammask model, cfg and device.

        Parameters
        ----------
        args
            Argparse namespace object.

        Returns
        -------
        siammask
            Siammask model.
        cfg
            Siammask configuration.
        device
            Torch device.
        """
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        torch.backends.cudnn.benchmark = True
        cfg = load_config(self.args)
        siammask = Custom(anchors=cfg['anchors'])
        if self.args.resume:
            assert isfile(self.args.resume), 'Please download {} first.'.format(self.args.resume)
            siammask = load_pretrain(siammask, self.args.resume)
        siammask.eval().to(device)
        self.siammask = siammask
        self.cfg = cfg
        self.device = device
        return siammask, cfg, device

    def video(self, path):
        """
        Return path VideoCapture.

        Parameters
        ----------
        path : str
            Path to video file.

        Returns
        -------
        cv2.VideoCapture
        """
        return cv2.VideoCapture(path)

    def camera(self):
        """
        Select and return live camera VideoCapture.

        Returns
        -------
        cv2.VideoCapture
        """
        """
        index = 0  # List available cameras i and numbers
        not_end = True
        while not_end:
            cap = cv2.VideoCapture(index + cv2.CAP_DSHOW)
            if not cap.read()[0]:
                if index != 0:
                    not_end = False
                else:
                    input('No camera detected, connect camera and press "Enter".')
                    index -= 1
            else:
                print('Camera number ' + str(index) + ' available')
            cap.release()
            index += 1
        cam_num = 0
        if index != 2:
            cam_num = input('Type camera number (eg. 0): ')
        else:
            print('The only one available camera number 0 is selected')
        """
        return cv2.VideoCapture(0)  # Connect with camera

    def process_one_frame(self):
        """
        Process only one frame (image) and shows / saves result.

        Parameters
        ----------
        args
            Argparse namespace object.

        Returns
        -------
        None
        """
        siam_mask = SiamMask(self.siammask, self.cfg, self.device, save_first_frame=self.args.save)
        image = cv2.imread(self.args.one_frame_path)
        cv2.namedWindow(self.window_name, cv2.WND_PROP_FULLSCREEN)
        cv2.imshow(self.window_name, image)
        selected_ROI = self.select_ROI(image, siam_mask)
        if not selected_ROI:
            print("Didn't select any ROI, abort")
        siam_mask.process(image)
        start_tick = cv2.getTickCount()
        siam_mask.process(image)
        print(
            "First frame processing time: {:02.1f}s".format((cv2.getTickCount() - start_tick) / cv2.getTickFrequency()))
        exit()

    def initialize_and_run(self):
        """
        Main handling function/loop.

        Parameters
        ----------
        args
            Argparse namespace object.

        Returns
        -------
        None
        """
        cv2.namedWindow(self.window_name, cv2.WND_PROP_FULLSCREEN)
        toc = 0
        f = 0
        self.print_info()
        ret, self.frame = self.capture_frame()
        cv2.imshow(self.window_name, self.frame)
        while self.cap.isOpened():
            if not self.stop:
                ret, self.frame = self.capture_frame()
            if ret and not self.stop:
                tic = cv2.getTickCount()
                for siam_mask in self.objects:
                    success, self.frame = siam_mask.process(self.frame)
                    if not success:
                        break
                cv2.imshow(self.window_name, self.frame)
                toc += cv2.getTickCount() - tic
            elif not ret:
                print("Couldn't get frame, exiting")
                break

            for siam_mask in self.objects:
                if siam_mask.state and "ploygon" in siam_mask.state:
                    if ((siam_mask.state['ploygon'][:, 0] < 0).any() or (
                            siam_mask.state['ploygon'][:, 0] > self.frame.shape[1]).any() or
                            (siam_mask.state['ploygon'][:, 1] < 0).any() or (
                                    siam_mask.state['ploygon'][:, 1] > self.frame.shape[0]).any()):
                        print("Object out of frame, please select new")
                        if self.args.bounds:
                            siam_mask.state = None
            self.objects[:] = [item for item in self.objects if item.state]
            if not self.objects and not self.start and not self.stop:
                print("Lost all objects, select new one")
                self.print_info()
                self.start = True
            key = cv2.waitKey(1)
            if key & 0xFF == ord('x') and not self.args.camera:
                self.toggle_play()
            elif key & 0xFF == ord('q'):
                print("Exited program")
                break
            elif key & 0xFF == ord('s'):
                print(self.frame.shape)
                window_open = self.select_new_ROI(self.frame, self.stop)
                if window_open and self.start:
                    self.start = False
            elif key & 0xFF == ord('r'):
                if not self.objects:
                    print("Nothing to reset")
                else:
                    self.objects = []
                    print("Reseted all objects")
                self.print_info()
                self.start = True
            """
            if cv2.getWindowProperty(self.window_name, cv2.WND_PROP_VISIBLE) < 1:
                print("Closed window")
                break
            """
            f += 1
        self.cap.release()
        cv2.destroyAllWindows()

        if f != 0 and toc != 0:
            toc /= cv2.getTickFrequency()
            fps = f / toc
            print('SiamMask Time: {:02.1f}s Speed: {:3.1f}fps (with visulization!)'.format(toc, fps))

    def print_info(self):
        """
        Print keyboard keys info for user.

        Parameters
        ----------
        args
            Argparse namespace object.

        Returns
        -------
        None
        """
        if self.args.camera:
            print("Select object (press s), exit (press q) or reset (press r)")
        else:
            print("Select object (press s), start/stop video (press x), exit (press q) or reset (press r)")

    def capture_frame(self):
        """
        Capture current VideoCapture frame.

        Returns
        -------
        ret : bool
            If True frame was captured successfully.
        frame : numpy.ndarray
            cv2 frame.
        """
        ret, frame = self.cap.read()
        return ret, frame

    def select_ROI(self, frame, siam_mask):
        """
        Select ROI using cv2 method. Opens interface that allow to select traced object ROI.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        siam_mask
            SiamMask class instance.

        Returns
        -------
        bool
            True if selection was successful.
        """
        try:
            init_rect = cv2.selectROI(self.window_name, frame, False, False)
            if init_rect != (0, 0, 0, 0):
                siam_mask.set_target_pos(init_rect)
                return True
            else:
                return False
        except Exception as e:
            print("Error:", e)
            return False

    def select_new_ROI(self, frame, stop):
        """
        Create new SiamMask object and initialize selection.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame
        stop : bool
            True if play is paused.
        args
            Argparse namespace object.

        Returns
        -------
        bool
            True if selection was successful.
        """
        random_colour = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]
        siam_mask = SiamMask(self.siammask, self.cfg, self.device, mask_colour=random_colour, bbox_colour=random_colour, mask_opacity=0.4,
                            save_first_frame=self.args.save)
        selected_ROI = self.select_ROI(frame, siam_mask)
        if selected_ROI:
            if stop:
                success, _ = siam_mask.process(frame)
                if not success:
                    print("Couldn't process SiamMask object")
            self.objects.append(siam_mask)
            if self.args.camera:
                print("Selected new ROI, select another one (press s), exit (press q) or reset (press r)")
            else:
                print(
                    "Selected new ROI, select another one (press s), start/stop video (press x), exit (press q) or reset (press r)")
            return True
        else:
            if self.args.camera:
                print("Didn't select any new ROI, try again (press s), exit (press q) or reset (press r)")
            else:
                print(
                    "Didn't select any new ROI, try again (press s), start/stop video (press x), exit (press q) or reset (press r)")
            return False


class Args:
    """
    ## Wrapper around argparse which allow to use siammask in GUI.

    Attributes
    ----------
    parser
        Argparse object.

    Parameters
    ----------
    filePath : str
        Video file path.
    imageChosen : bool
        If True saves processed image.
    davisPthPath : str
        Path to pretrained model.
    davisJsonPath : str
        Path to model configuration (in JSON).
    """
    def __init__(self, filePath, imageChosen, davisPthPath, davisJsonPath):
        self.parser = argparse.ArgumentParser(description='PyTorch Tracking Demo')
        self.parser.add_argument('--resume', default=davisPthPath, type=str,
                            metavar='PATH', help='path to latest checkpoint (default: none)')
        self.parser.add_argument('--config', dest='config', default=davisJsonPath,
                            help='hyper-parameter of SiamMask in json format')
        self.parser.add_argument('--video', default=filePath, help='video path')
        self.parser.add_argument('--camera', action='store_true', help='use live camera')
        self.parser.add_argument('--cpu', action='store_true', help='cpu mode')
        self.parser.add_argument('--bounds', action='store_true', help='handle object out of frame')
        self.parser.add_argument('--save', action='store_true', help='save first frame, mask and bbox as png')
        self.parser.add_argument('--save_path', default='out', type=str, help='path to output save directory (with file prefix)')
        self.parser.add_argument('--one_frame', default=imageChosen, action='store_true', help='save process image')
        self.parser.add_argument('--one_frame_path', default=filePath, type=str, help='path to image')

    def parse_args(self, list_of_args):
        """
        Parse arguments in array to argparse.

        Parameters
        ----------
        list_of_args : list
            Array of arguments.
        
        Returns
        -------
        args
            Argparse namespace object.
        """
        return self.parser.parse_args(list_of_args)



if __name__ == "__main__":

    list_of_args = []  # lista z argumentami, zmieniana w GUI
    args_class = Args()
    args = args_class.parse_args(list_of_args)

    # Parse Image file
    if not args.camera:
        path = args.video
    else:
        path = ''

    handling = SiamHandling(path)
    siammask, cfg, device = handling.setup_model()

    if args.one_frame:
        handling.process_one_frame()
    else:
        handling.initialize_and_run()

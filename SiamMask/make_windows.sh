#!/usr/bin/env bash

cd experiments/siammask_sharp/tools/utils/pyvotkit
python setup.py build_ext --inplace 
cd ../

cd pysot/utils/
python setup.py build_ext --inplace
cd ../../../

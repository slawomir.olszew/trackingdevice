## GUI (Nikita, Piotr):
1. mozliwosc wybrania filmu, 
2. mozliwosc wybrania metody segmentacji,
3. start filmu
4. zatrzymanie filmu  i wybranie obiektu na pojedynczej klatce(metoda MaskRCNN i SiamMask), dla metody ICNet segmentacja na calym obrazie i 							 
wybranie interesujacego nas obiektu(kilikajac w dana maske), 
5. kontynuuacja filmu wraz z odpalonym juz trackerem i zaznaczonym BoundingBoxem na obiekcie - dla MaskRCNN i ICNet niech to bedzie orb 	 
        descriptor + optical flow lub tracker z opencv/dlib, SiamMask ma w sobie wbudowany tracker, mozemy tego uzyc

## MaskRCNN (Krzysztof):
1. w pliku rcnn.py jest opracowana gotowa juz klasa i metoda get_masked_frame ktora zwroci maske obiektu potrzebna do dalszego sledzenia,
	wydaje mi sie ze nie ma w tej metodzie juz nic wiecej do dodania/poprawy, jak wszystko bedzie spiete trzeba bedzie poczyscic folder z plikow ktore 		      nie sa potrzebne

## ICNet (Marta, Maciej):
1. plik ICNet_img.py trzeba przerobic na klase, zeby wszystkie konfiguracje byly ladowane wraz z jej wywolaniem(potrzebne jak importujemy ten plik do innego skryptu), 
2. wyjscie metody Processing przerobic na takie same jak z MaskRCNN - oryiginalny obraz z nalozonymi maskami, tablica masek, czas jaki zajelo przeliczanie zdjecia

## SiamMask (Darek, Paweł, Sebastian):
1. w pliku experiments/siammask_sharp/siam_mask.py setup_model trzeba wrzucic do sroda klasy SiamMask , zeby nie bylo problemu z 	 
        importowaniem, 
2. argparsa zastapic zwyklymi argumentami w inicjalizacji klasy SiamHandling lub w ogole je wywalic i tylko do process_one_frame przekazac wyciety z GUI obiekt z jednego frame(odpalamy z innego pliku a nie z konsoli), 
3. Pytanie czy z tej jednej wybranej klatki i zaznaczonego obiektu da rade dalej korzystac z wbudowanego trackera i przekazywac Bounding Boxa jako 	 return?

## Do zrobienia z nowych rzeczy:
1. **Rafał** - Opracować algorytm sledzenia optical flow & orb descriptor -> w projekcie 007 w folderze serwer/Yolo jest taki plik juz, mozna na nim bazowac, sprawdzic dzialanie i ewentualnie dopracowac, sledzi obraz wyciety za pomoca masek zwroconych z metod segmentacji, zwraca polozenie i wymiary boundingBoxa
2. **Marek** - Opracować tracker z opencv/dlib -> jest gotowy w projekcie 007, podobnie jak wyzej, sledzimy obraz wyciety za pomoca masek zwroconych z metod semgnetacji, zwracamy polozenie i wymiary bounding boxa
3. **Nikita, Piotr** -  Opracowac requirements i instrukcje do inicjalizacji srodowiska calego projektu, tak zeby odpalajac GUI wszystkie metody mialy spelnione wymagania i bez problemu sie uruchomily
	


	
"""
### Tracker module realizing optical flow + orb descriptor method. Can be used with [MaskRCNN](mask_r_cnn.rcnn.html) and [ICNet](ICNet_tensorflow_master.ICNet.html)
"""
import sys
from pathlib import Path
import os
import tensorflow as tf
tf.disable_v2_behavior()
config=tf.ConfigProto()
config.gpu_options.allow_growth=True
sess=tf.Session(config=config)
import cv2
import time
import numpy as np

def orb(cap, mask, old_frame):
    """
    Main function of optical flow + orb descriptor tracking method. Uses [OpenCV ORB](https://docs.opencv.org/4.4.0/d1/d89/tutorial_py_orb.html) 
    descriptor and [OpenCV Optical Flow](https://docs.opencv.org/4.4.0/d4/dee/tutorial_optical_flow.html) implementations.

    Parameters
    ----------
    cap : cv2.VideoCapture
        Object used for video capturing from video files, image sequences or cameras.
    mask : numpy.ndarray
        Mask of an object to track/tracked object.
    old_frame: numpy.ndarray
        Initial cv2 frame.
        
    Returns
    -------
    None
    """
    lk_params = dict(winSize=(100, 100), maxLevel=5, criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
    old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
    img = cv2.bitwise_and(old_gray, old_gray, mask=mask)
    detector = cv2.ORB_create(1000, edgeThreshold=15)
    kp = detector.detect(img)
    p0 = cv2.KeyPoint_convert(kp)
    p0 = p0.reshape(-1, 1, 2)

    while cap.isOpened():
        key = cv2.waitKey(16) & 0xFF
        ret, frame = cap.read()
        if key == ord('q'):
            ret = None
        if ret:
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
            good_new = p1[st == 1]
            good_old = p0[st == 1]

            filtered = good_old.astype(np.int)

            x_sorted = sorted(filtered, key=lambda k: k[0])
            y_sorted = sorted(filtered, key=lambda k: k[1])
            x1, x2 = x_sorted[0][0], x_sorted[-1][0]
            y1, y2 = y_sorted[0][1], y_sorted[-1][1]

            frame = cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 2)
            X1, X2, Y1, Y2 = x1, x2, y1, y2

            cv2.imshow('Frame', frame)
            old_gray = frame_gray.copy()
            p0 = good_new.reshape(-1, 1, 2)
        else:
            cv2.destroyAllWindows()
            cap.release()
import os
import sys
import time
import cv2
from rcnn import MaskRCNN

rcnnClass = MaskRCNN()
im_paths = ['./images/1045023827_4ec3e8ba5c_z.jpg', './images/2516944023_d00345997d_z.jpg',
            './images/8053677163_d4c8f416be_z.jpg']
for path in im_paths:
    img = cv2.imread(path, 1)
    im, masks,elapsedTime = rcnnClass.get_masked_frame(img)

    cv2.imshow("masked_img", im)
    cv2.waitKey(0)

# boundingBox,clases,names=rcnnClass.processFrame(img)
# rcnnClass.showFrame(img,boundingBox)

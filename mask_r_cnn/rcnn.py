"""
### Segmentation module using [Mask R-CNN](https://github.com/matterport/Mask_RCNN).
"""
import os
import pathlib
import sys

ROOT_DIR = os.path.abspath(pathlib.Path(__file__).parent.absolute())
sys.path.append(ROOT_DIR)  # To find local version of the library
import time
import cv2
import tensorflow as tf
import warnings
from mrcnn import utils
from mrcnn import model as modellib
from mrcnn import visualize
from model.coco import coco
from cocoClassNames import class_names
import random
from skimage import transform
import numpy as np

warnings.filterwarnings("ignore")


class InferenceConfig(coco.CocoConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    MAX_GT_INSTANCES = 100
    TRAIN_ROIS_PER_IMAGE = 50
    RPN_ANCHOR_STRIDE = 4  # better performance, but less objects detected
    POST_NMS_ROIS_TRAINING = 1000
    POST_NMS_ROIS_INFERENCE = 1000
    IMAGE_MIN_DIM = 100  # really much faster but bad results
    IMAGE_MAX_DIM = 512
    DETECTION_MAX_INSTANCES = 50  # a little faster but some instances not recognized


class MaskRCNN:
    """
    ## MaskRCNN class, contains all necessary methods.

    Attributes
    ----------
    CURRENT_PATH : pathlib.Path
        Path to this file adequate to used operating system.
    MODEL_DIR : str
        Path to model directory (output).
    COCO_MODEL_PATH : str
        Path to coco model in h5 format.
    scaleFactor : float
        Variable used to scale frame (image).
    ## Attributes outside __init__ (`MaskRCNN.set_configuration`)
    config
        `InferenceConfig` object
    """
    def __init__(self):
        self.CURRENT_PATH = pathlib.Path(__file__).parent.absolute()
        self.MODEL_DIR = os.path.join(self.CURRENT_PATH, 'model', "logs")
        self.COCO_MODEL_PATH = os.path.join(self.CURRENT_PATH, 'model', "mask_rcnn_coco.h5")
        self.scaleFactor = 0.5
        self.initialize()

    def initialize(self):
        """
        Set MaskRCNN configuration and load coco model.
        
        Returns
        -------
        None
        """
        self.set_configuration()
        self.load_model()

    def set_configuration(self):
        """
        Configure training parameters using `InferenceConfig`.
        
        Returns
        -------
        None
        """
        self.config = InferenceConfig()

    def load_model(self):
        """
        Download COCO trained weights (if model can't be found) and load model (weights).
        
        Returns
        -------
        None
        """
        # Download COCO trained weights from Releases if needed
        if not os.path.exists(self.COCO_MODEL_PATH):
            utils.download_trained_weights(self.COCO_MODEL_PATH)
        self.model = modellib.MaskRCNN(mode="inference", config=self.config, model_dir=self.MODEL_DIR)
        # Load weights trained on MS-COCO
        self.model.load_weights(self.COCO_MODEL_PATH, by_name=True)

    def scaleFrame(self, frame):
        """
        Resize frame using `cv2.resize`.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        copiedFrame : numpy.ndarray
            Resized frame.
        """
        copiedFrame = cv2.resize(frame.copy(), None, fx=self.scaleFactor, fy=self.scaleFactor,
                                 interpolation=cv2.INTER_AREA)
        return copiedFrame

    def processFrame(self, frame):
        """
        Process frame (detect objects), alternative to `MaskRCNN.get_masked_frame`. Not used.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        upScaledRois : list(list(int))
            Detected, upscaled ROIs.
        r['class_ids'] : list(int)
            Detected objects ids.
        names : list(str)
            Detected object names (COCO).
        """
        start = time.perf_counter()
        downScaledFrame = self.scaleFrame(frame)
        results = self.model.detect([downScaledFrame])
        stop = time.perf_counter()
        print('Processed frame in ', stop - start, 'seconds')
        r = results[0]
        names = []
        for ids in r['class_ids']:
            names.append(class_names[ids])
        upScaledRois = []
        for roi in r['rois']:
            subRow = []
            for elem in roi:
                subRow.append(int(elem * (1 / self.scaleFactor)))
            upScaledRois.append(subRow)
        return upScaledRois, r['class_ids'], names

    def showFrame(self, frame, boxes):
        """
        Show frame and draw bouding boxes on it. Not used.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        boxes : list(list(int))
            Bounding boxes coordinates
        """
        for box in boxes:
            cv2.rectangle(frame, (box[3], box[2]), (box[1], box[0]),
                          color=(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)), thickness=3)
        cv2.imshow("Output frame", frame)
        cv2.waitKey(0)

    def get_masked_frame(self, frame):
        """
        Main MaskRCNN function which detects objects on frame and returns results in proper form.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        masked_image : numpy.ndarray
            Image (frame) with applied masks of detected objects.
        frame_masks: list(numpy.ndarray)
            Detected objects masks.
        totalTime : float
            Total time which was taken to detect objects on frame.
        """
        t_before = time.perf_counter()
        results = self.model.detect([frame])[0]
        t_after = time.perf_counter()
        totalTime=t_after - t_before
        print('Processed frame in ', totalTime, 'seconds')

        N = results['rois'].shape[0]

        colors = visualize.random_colors(N)

        masked_image = frame.copy()
        masks = results['masks']
        frame_masks = []
        for i in range(N):
            mask = masks[:, :, i]
            frame_masks.append(mask)
            masked_image = visualize.apply_mask(masked_image, mask, colors[i])

        return masked_image, frame_masks,totalTime

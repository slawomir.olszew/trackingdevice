import os
import sys
import time
import cv2
import tensorflow as tf
import warnings
# Import Mask RCNN
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize

warnings.filterwarnings("ignore")

# Import COCO config
sys.path.append(os.path.join('model', "coco/"))  # To find local version
import coco
from cocoClassNames import class_names

# Directory to save logs and trained model
MODEL_DIR = os.path.join('model', "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join('model', "mask_rcnn_coco.h5")

# Download COCO trained weights from Releases if needed
if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
IMAGE_DIR = os.path.join("images")


class InferenceConfig(coco.CocoConfig):
    # class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


config = InferenceConfig()
config.STEPS_PER_EPOCH = 200
config.TOP_DOWN_PYRAMID_SIZE = 256

config.display()

model = modellib.MaskRCNN(mode="inference", config=config, model_dir=MODEL_DIR)
# Load weights trained on MS-COCO

model.load_weights(COCO_MODEL_PATH, by_name=True)

file_names = next(os.walk(IMAGE_DIR))[2]

images = []

for idx, file_name in enumerate(file_names):
    if idx < 1:
        img = cv2.imread(os.path.join(IMAGE_DIR, file_name))
        if img is not None:
            images.append(img)

results = []
# Run detection
for im in images:
    t_before = time.perf_counter()
    results.append(model.detect([im]))
    t_after = time.perf_counter()
    print("Execution time: ", t_after - t_before)

# Visualize results
i = 0
for result in results:
    r = result[0]
    visualize.display_instances(images[i], r['rois'], r['masks'], r['class_ids'],
                                class_names, r['scores'])
    i += 1

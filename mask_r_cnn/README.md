# Przykład Mask-r-cnn 
Plik **maskrcc_example.py** zawiera przykład działania sieci mask-r-cnn
Przed uruchominiem należy zainstalować wymagane moduły:
```python
pip install requirements.txt
```
Przykładowe zdjęcie przed analizą sieci:<br/>
![](test_image_before.jpg)<br/>
Przykładowe zdjęcie po analizie sieci:<br/>
![](test_image_after.png)
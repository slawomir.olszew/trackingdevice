"""
### Segmentation module using [ICNet - tensorflow implementation](https://github.com/hellochick/ICNet-tensorflow).
"""
import argparse
import tensorflow as tf
import numpy as np
import time
from tqdm import trange
import sys
import cv2
from .utils.config import Config

from .model import ICNet, ICNet_BN

class Configuration(Config):
    """
    ## Configuration class, derivates from custom ICNet config structure.

    Attributes
    ----------
    m_type : str
        Key of model to use (ICNet).
    m_weight : str
        Path to model file.
    IMG_SIZE : tuple(int)
        Image dimensions.
    """
    def __init__(self, dataset, is_training, filter_scale, modelPath):
        Config.__init__(self, dataset, is_training, filter_scale)
        self.m_type = 'trainval'
        self.m_weight = modelPath
        # Rozmiar obrazu podawanego na wejscie
        self.IMG_SIZE = (1024, 2048, 3)

class IcnetClass:
    """
    ## ICNet class, contains all necessary methods.

    Attributes
    ----------
    modelPath : str
        Path to model in .npy format.
    m_configuration : dict
        Data structure with available ICNet options (models), currently, 'trainval' is used.
    data_set : str
        Chosen data set, currently, 'cityscapes' is used.
    filter_scale : int
        Variable used in ICNet configuration.
    args
        Argparse namespace object.
    vid : bool
        Set to true (video processing). Not used.
    ## Attributes outside __init__ (`IcnetClass.create_session`)
    conf
        `Configuration` object.
    model
        Model selected from `IcnetClass.m_configuration`.
    net
        Initialized ICNet model.
    """
    def __init__(self, modelPath, m_configuration, data_set, filter_scale, args):
        self.modelPath = modelPath
        self.m_configuration = m_configuration
        self.data_set = data_set
        self.filter_scale = filter_scale
        self.args = args
        self.initialize_model()
        self.vid = True

    def initialize_model(self):
        """
        Set ICNet configuration and create session (initialize model).
        
        Returns
        -------
        None
        """
        self.set_config()
        self.create_session()

    def set_config(self):
        """
        Set ICNet configuration with user-set parameters.
        
        Returns
        -------
        None
        """
        self.conf = Configuration(self.data_set, is_training=False, filter_scale=self.filter_scale, modelPath=self.modelPath)

    def create_session(self):
        """
        Initialize and create session of selected ICNet model.
        
        Returns
        -------
        None
        """
        self.model = self.m_configuration[self.conf.m_type]
        self.net = self.model(cfg=self.conf, mode='inference')
        self.net.create_session()
        self.net.restore(self.conf.m_weight)

    def scale_image(self, frame):
        """
        Resize frame using `cv2.resize` with `Configuration.IMG_SIZE` dimensions.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        scaled_img : numpy.ndarray
            Resized frame.
        """
        scaled_img = cv2.resize(frame.copy(), (self.conf.IMG_SIZE[1], self.conf.IMG_SIZE[0]))
        return scaled_img

    def processing_img(self, frame):
        """
        Process frame (image) with ICNet model and calculate processing time. Used when ICNet is used separately (standalone).

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        out : numpy.ndarray
            Output frame (with masks).
        duration : float
            Time which was taken to process (predict) objects on frame with ICNet model.
        mask : numpy.ndarray
            Mask (masks) of detected object (objects).
        """
        img = cv2.imread(frame)
        img_resized = self.scale_image(img)

        start_t = time.time()
        result = self.net.predict(img_resized)
        duration = time.time() - start_t

        mask = result[0].astype('uint8')
        overlap_res = 0.5 * img_resized + 0.5 * result[0]
        out = overlap_res
        return out, duration, mask

    def get_masks(self, frame):
        """
        Main function, process video frame with ICNet model.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        out : numpy.ndarray
            Output frame (with masks).
        mask : numpy.ndarray
            Mask (masks) of detected object (objects).

        Notes
        -----
        Unfortunately, images (single frame) can't be processed in GUI with ICNet.
        """
        if type(frame) is bool:
            print("Can't process image (single frame)")
            return None, None
        else:
            img_resized = self.scale_image(frame)

            result = self.net.predict(img_resized)
            mask = result[0].astype('uint8')
            out = cv2.addWeighted(img_resized, 0.5, mask, 0.5, 0)
            return out, mask

    def image_handling(self, frame):
        """
        Function used to handle frame (image) processing if ICNet is used separately (standalone). Shows processing time.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        None
        """
        output, duration, mask = self.processing_img(self.args.image)
        cv2.imwrite(self.args.result_img, output)
        print('Processing time: {:.4f} s, about {:.6f} fps'.format(duration, 1 / duration))
        im = cv2.imread(self.args.result_img)
        im_r = cv2.resize(im, (480, 260))
        x1, y1, w1, h1 = cv2.selectROI('ICNet Result', im_r, False)
        cv2.rectangle(im_r, (x1, y1), (x1 + w1, y1 + h1), (0, 255, 0), 2)

        cv2.imshow('ICNet Result', im_r)
        cv2.waitKey(0)
        key1 = cv2.waitKey(1) & 0xFF
        if key1 == ord('q'):
            cv2.destroyWindow('ICNet Result')

    def processing_vid(self, frame):
        """
        Function used to process single video frame if ICNet is used separately (standalone).

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        out : numpy.ndarray
            Output frame (with masks).
        duration : float
            Time which was taken to process (predict) objects on frame with ICNet model.
        mask : numpy.ndarray
            Mask (masks) of detected object (objects).
        """
        img_resized = self.scale_image(frame)

        start_t = time.time()
        result = self.net.predict(img_resized)
        duration = time.time() - start_t

        mask = result[0].astype('uint8')
        overlap_res = (0.5 * img_resized + 0.5 * result[0])
        out = overlap_res.astype('uint8')
        return out, duration, mask

    def video_handling(self, frame):
        """
        Function used to handle video processing if ICNet is used separately (standalone). Shows processing time.

        Parameters
        ----------
        frame
            Path to video file or camera selection (int).
        
        Returns
        -------
        None
        """
        vid = cv2.VideoCapture(frame)
        while vid.isOpened():
            ret, frame = vid.read()
            if not ret:
                break

            output, duration, mask = self.processing_vid(frame)
            print('Processing time: {:.4f} s, about {:.6f} fps'.format(duration, 1 / duration))
            cv2.imshow("Frame", output)

            key = cv2.waitKey(1) & 0xFF

            if key == ord("q"):
                break

def createICNETInstance(modelPath, filePath, videoChosen):
    """
    Function responsible for creating ICNet instance (configuration, initialization) and getting its results (`IcnetClass.get_masks`). Used in [GUI](gui.html). 

    Parameters
    ----------
    modelPath : numpy.ndarray
        cv2 frame.
    filePath : str
        Path to image or video.
    videoChosen : numpy.ndarray
        Frame of the video.

    Returns
    -------
    out : numpy.ndarray
        Output frame with masks.
    mask : numpy.ndarray
        Array containing mask (masks).
    """
    # Konfiguracja modelu
    m_configuration = {'train': ICNet, 'trainval': ICNet, 'train_bn': ICNet_BN, 'trainval_bn': ICNet_BN,
                       'others': ICNet_BN}

    # Wybór bazy danych
    data_set = 'cityscapes'
    # data_set = 'ade20k'
    filter_scale = 1

    parser = argparse.ArgumentParser()
    parser.add_argument('--image', default=filePath, help='image path')
    parser.add_argument('--video', default=filePath, help='video path')
    parser.add_argument('--result_img', default=r'./data/output/saves_res.png', help='processed img')
    args = parser.parse_args()

    icnet_class = IcnetClass(modelPath, m_configuration, data_set, filter_scale, args)

    return icnet_class.get_masks(videoChosen)

import argparse
import tensorflow as tf
import numpy as np
import cv2
import time
from tqdm import trange
import sys
from utils.config import Config
from model import ICNet, ICNet_BN

# Konfiguracja modelu
m_configuration = {'train': ICNet, 'trainval': ICNet, 'train_bn': ICNet_BN, 'trainval_bn': ICNet_BN, 'others': ICNet_BN}

# Wybór bazy danych
data_set = 'cityscapes'
# data_set = 'ade20k'
filter_scale = 1


class Configuration(Config):
    def __init__(self, dataset, is_training, filter_scale):
        Config.__init__(self, dataset, is_training, filter_scale)

    m_type = 'trainval'
    m_weight = r'./model/cityscapes/icnet_cityscapes_trainval_90k.npy'

    # Rozmiar obrazu podawanego na wejscie
    IMG_SIZE = (1024, 2048, 3)


class IcnetClass():
    def __init__(self):
        self.initialize_model()

    def initialize_model(self):
        self.set_config()
        self.create_session()

    def set_config(self):
        self.conf = Configuration(data_set, is_training=False, filter_scale=filter_scale)

    def create_session(self):
        self.model = m_configuration[self.conf.m_type]
        self.net = self.model(cfg=self.conf, mode='inference')
        self.net.create_session()
        self.net.restore(self.conf.m_weight)

    def scale_image(self, frame):
        scaled_img = cv2.resize(frame.copy(), (self.conf.IMG_SIZE[1], self.conf.IMG_SIZE[0]))
        return scaled_img

    def processing_img(self, frame):
        img = cv2.imread(frame)
        self.img_resized = self.scale_image(img)
        start_t = time.time()
        result = self.net.predict(self.img_resized)
        duration = time.time() - start_t

        overlap_res = 0.5 * self.img_resized + 0.5 * result[0]
        out = overlap_res
        return out, duration

    def processing_vid(self, frame):
        self.img_resized = self.scale_image(frame)
        start_t = time.time()
        result = self.net.predict(self.img_resized)
        duration = time.time() - start_t

        overlap_res = 0.5 * self.img_resized + 0.5 * result[0]
        out = overlap_res / 255.0
        return out, duration

    def video_handling(self, frame):
        vid = cv2.VideoCapture(frame)
        while True:
            (ret, frame) = vid.read()
            if not ret:
                break
            output, duration = self.processing_vid(frame)
            cv2.imshow("Frame", output)

            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                break


parser = argparse.ArgumentParser()
parser.add_argument('--image1', default=r'./data/input/cityscapes1.png')
parser.add_argument('--video', default=0, help='video path')
parser.add_argument('--result1', default=r'./data/output/saves_res.png')
args = parser.parse_args()

icnet_class = IcnetClass()

vid = False
if vid:
    icnet_class.video_handling(args.video)
else:
    output_1, duration_1 = icnet_class.processing_img(args.image1)

    print('Processing time: {:.4f} s, about {:.6f} fps'.format(duration_1, 1 / duration_1))

    cv2.imwrite(args.result1, output_1)

    im = cv2.imread(args.result1)
    im_r = cv2.resize(im, (960, 540))
    x, y, w, h = cv2.selectROI('ICNet Result', im_r, False)
    cv2.rectangle(im_r, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cv2.imshow('ICNet Result', im_r)
    cv2.waitKey(0)
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        cv2.destroyWindow('ICNet Result')

# ICNet_tensorflow  
Skrypty **ICNet_img.py** oraz **ICNet.py** pozwalają na wygenerowanie maski segmentacji dla każdego piksela w obrazie, przy czym pierwszy pracuje na zdjęciach natomiast drugi na obrazie z kamery lub wcześniej przygotowanych nagraniach wideo.

## Przygotowanie środowiska
```
pip install -r ICNet-tensorflow-master/requirements.txt
```

## Pobranie modelu
```
python script/download_weights.py --dataset cityscapes 
```

## Rezultaty osiągnięte na obrazach przykładowych

 ![](./utils/ICNet_result.PNG)

 Jak widać powyżej program po wykonaniu operacji na obrazie zwraca go na wykresie w 3 postaciach.
 Zaczynając od lewej: 

| Oryginalny obraz | Maski segmentacji | Obraz po segmentacji z nałożonymi maskami |
| ------------- |:-------------:| -----:|

Do wykresu dodany został również czas trwania przetwarzania, jak można zauważyć urządzenie na którym przeprowadzany był test nie zostało stworzone do prac tego typu, co sprawia, iż sprawnie uniemożliwia przeprowadzenie testów w czasie rzeczywistym. 

## Rezultaty osiągnięte na nagraniach wideo
 ![](./data/output/rezultaty.gif)

Powyższa animacja przedstawia rezultaty działania programu przy czterech różnych podejściach. Zastosowanie trackera na nagraniu niepoddanym operacji segmentacji z wykorzystaniem ICNet. Analogicznie jak w pierwszym przypadku. Do samej wizualizacja natomiast zastosowane zostały połączone ramki: oryginalna oraz po dokonaniu segmentacji. Zastosowanie śledzenia na ramkach prezentujących tylko i wyłącznie maski wykrytych obiektów. Śledzenie odbywa się na ramkach wyświetlanych na ekranie (50% oryginalnej ramki + 50% ramki z uzyskanymi maskami)

Rezultaty poszczególnych podejść zaprezentowane w animacji reprezentują czas rzeczywisty, natomiast aby osiągnąć taki efekt nagrania zostały znacznie przyspieszone, 1. około 4 krotnie, zaś pozostałe ponad 40 krotnie. Taki zabieg był konieczny w związku z niezoptymalizowaną segmentacją ICNet oraz niewystarczającą mocą obliczeniową zapewnioną przez kartę graficzną. Średni czas przetwarzania jednej klatki przez ICNet = 2.3173 s, co daje około 0.431530 fps.



 Linki do filmów z naszych prac:
- tracker (oryginał)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://youtu.be/-tuzFf45oos
- tracker (oryginał, wyświetlany z segmentacją)&nbsp;&nbsp;&nbsp;https://youtu.be/IAy_Pj9jllA
- tracker (maska)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://youtu.be/cDzLgcB4kdY
- tracker (50% oryginał, 50% maska)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://youtu.be/RkRfnIJ96H4
  
## Przegląd dostępnych metod śledzenia (biblioteka opencv)

Poszukując optymalnego trackera dostępnego w bibliotece opencv oraz działającego poprawnie na efektach otrzymanych w wyniku segmentacji, wypróbowaliśmy następujące trackery: 

1. **BOOSTING Tracker**: Metoda śledzenia oparta na tym samym algorytmie, który napędza uczenie maszynowe stojące za klasyfikatorem Haar cascades (AdaBoost), niestety podobnie jak wspomniany klasyfikator liczy sobie ponad dekadę. Tracker kiepsko radził sobie podczas testów na nagraniach pojazdów w środowisku miejskim.
2. **MIL Tracker**: Tracker dokładniejszy aniżeli Boosting Tracker. Radzi sobie słabo ze zwracaniem błędów. Efekty działania oceniliśmy na niewystarczające dla naszego projektu.
3. **KCF Tracker**: Szybszy niż metody BOOSTING i MIL. Natomiast analogicznie jak w przypadku dwóch poprzednich metod nie radzi sobie z pełną okluzją. Jest podatny na gwałtowne ruchy śledzonych obiektów, zmiany wzorców ich wyglądu oraz sceny, a także ich elastyczne struktury.
4. **Median Flow Tracker**: Metoda nie radzi sobie w przypadku dużej zmienności śledzonego obiektu (np. zmiana kąta z którego obserwujemy obiekt poprzez zmianę jego położenia). 
5. **Śledzenie TLD**:  Metoda niezwykle podatna na wszelkiego rodzaju zakłócenia. Minimalna utrata śledzonego obiektu z pola widzenia powoduje zerwanie śledzenia.
6. **MOSSE Tracker**: Najszybszy z przetestowanych trackerów, natomiast nie tak dokładny jak KCF Tracker lub CSRT, co finalnie przemówiło na jego niekorzyść przy naszym wyborze.


## CSRT tracker
Discriminative Correlation Filter (with Channel and Spatial Reliability). W dyskryminacyjnym filtrze korelacji z niezawodnością kanałową i przestrzenną (DCF-CSR) wykorzystujemy mapę niezawodności przestrzennej do dostosowania obsługi filtru do części wybranego regionu z ramki do śledzenia. Zapewnia to powiększenie i lokalizację wybranego obszaru oraz ulepszone śledzenie obszarów lub obiektów, które nie są prostokątne. Używa tylko 2 standardowych funkcji (HoGs i Colornames). Działa również przy stosunkowo niższych fps (25 fps), ale zapewnia większą dokładność śledzenia obiektów. W naszym przykładzie został sprawdzony przy klatkowaniu 0.4 fps uzyskując skuteczne śledzenie obiektu.

## Specyfikacja komputera, na którym wykonywane były testy:

| Procesor | RAM | Karta graficzna |
| ------ | ------ | ----- |
| Intel Core i5-3210M 2.5GHz |  8 GB | NVIDIA GeForce GT 650M |


## Wnioski

Porównując metodę ICNet z innymi metodami segmentacji używanymi w naszym projekcie, bardzo wyraźnie widać, że ICNet nie jest tak dobrą metodą do tego zadania jak dwie pozostałe. Już na samym początku prac odnotowaliśmy, że ICNet nie ma wbudowanego trackera, w przeciwieństwie do siammask i RCNN, co zmuszało nas do dołączenia trackera, który nasz zespół stworzy. Wyraźnie rzuca się w oczy również sposób segmentacji, który wyróżnia ICNet na tle pozostałych metod. RCNN i Siammask, segmentując obraz, zwracały różne maski dla każdego z obiektów, niezależnie czy był to obiekt tej samej klasy co znalezione wcześniej obiekty. ICNet zwraca jedną maskę dla wszystkich obiektów tej samej klasy. W wyniku tego otrzymujemy jedną maskę dla wszystkich samochodów, co wizualnie przypomina rozlaną czerwoną farbę, zamiast zarysów samochodów jak jest to widoczne np. na maskach RCNN.

Wspomnieć należy też o optymalizacji metody i prędkości jej działania. Porównując ICNet z RCNN na tej samej maszynie i na tym samym nagraniu, zauważyć można było wyraźne spadki płynności używając ICNet do 4 fps. RCNN działa dużo płynniej, dodatkowo zwracając osobne maski dla każdego obiektu.

Wspólnie doszliśmy do wniosku, że ICNet jest dobrą metodą, ale do innych zastosowań. Zakładając, że dostępny byłby lepszy komputer lub udałoby się zoptymalizować działanie ICNet do stopnia, gdzie metoda działałaby płynnie, mogłaby służyć jako segmentacja w autach delegująca odpowiednie części obrazu do kolejnych metod np. klasyfikujących mijane znaki drogowe i informujących kierowcę o aktualnie panujących przepisach. W takim przypadku ICNet mógłby usprawnić działanie dalszych algorytmów, które nie musiałyby przeszukiwać całego obrazu np. w poszukiwaniu znaków drogowych, bo dostawałyby gotowe do analizy fragmenty obrazu.

Nie zaobserwowaliśmy większego wpływu segmentacji na pracę trackera CSRT. Na filmach i gifie można łatwo zauważyć, że tracker działał bardzo podobnie bazując na samych maskach, połączeniu oryginalnego obrazu z maskami i oryginalnym obrazem bez masek. Różnicę natomiast odczuwa osoba korzystająca z trackera połączonego z segmentacją, ponieważ segmentacja spowalniała działanie całego programu dziesięciokrotnie, nie dając jednocześnie, żadnych korzyści w postaci lepszej jakości śledzenia.

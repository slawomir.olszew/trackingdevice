URLS=[
"dokumentacja.html",
"gui.html",
"mask_r_cnn.rcnn.html",
"SiamMask.experiments.siammask_sharp.siam_mask.html",
"ICNet_tensorflow_master.ICNet.html",
"tracker.html",
"tracker_dlib.html",
"orb_tracker.html"
];
INDEX=[
{
"ref":"dokumentacja",
"url":0,
"doc":" Dokumentacja projektu [Tracking Device](https: gitlab.com/slawomir.olszew/trackingdevice)  Autorzy: [S\u0142awomir Olszewski](https: gitlab.com/slawomir.olszew) [Mykyta Brazhynskyy](https: gitlab.com/mykyta.brazhynskyy) [Krzysztof Jeschke](https: gitlab.com/jeschke.krzysztof44) [Rafa\u0142 Kaliszak](https: gitlab.com/rafalkaliszak) [Piotr Truty](https: gitlab.com/P.Truty) [Marta Gozdowska](https: gitlab.com/margoo9) [Marek Mikulski](https: gitlab.com/marek_mikulski) [Maciej Kura\u017c](https: gitlab.com/MKuraz) [Sebastian Lewalski](https: gitlab.com/LewalskiSebastian) [Dariusz Libecki](https: gitlab.com/dariuszlibecki) [Pawe\u0142 Niedziela](https: gitlab.com/pawel.n) TrackingDevice to projekt maj\u0105cy na celu opracowanie metod \u015bledzenia - segmentacj\u0119 semantyczn\u0105 obrazu (klatki filmu) a nast\u0119pnie \u015bledzenie wybranego obiektu lub obiekt\u00f3w. W tym celu zosta\u0142a stworzona aplikacja desktopowa w j\u0119zyku programowania Python. Mo\u017cna zatem podzieli\u0107 projekt na dwie cz\u0119\u015bci, gdzie zdecydowano si\u0119 na zaimplementowanie kilku metod (arbitralnie uznanych za sensowne i skuteczne) dla ka\u017cdej z nich: 1. segmentacja obrazu - [Mask R-CNN](https: github.com/matterport/Mask_RCNN) - [ICNet](https: github.com/hellochick/ICNet-tensorflow) - [SiamMask](https: github.com/foolwood/SiamMask) 2. \u015bledzenie obiektu/obiekt\u00f3w (trackery) - [optical flow](https: docs.opencv.org/4.4.0/d4/dee/tutorial_optical_flow.html) + [orb descriptor](https: docs.opencv.org/4.4.0/d1/d89/tutorial_py_orb.html) - [tracker z biblioteki Dlib (correlation tracker)](http: dlib.net/python/index.html dlib.correlation_tracker) - [trackery z biblioteki OpenCV](https: docs.opencv.org/4.4.0/d9/df8/group__tracking.html) (wybrany - [CSRT tracker](https: docs.opencv.org/4.4.0/d2/da2/classcv_1_1TrackerCSRT.html  GUI Centrum projektu jest graficzny interfejs u\u017cytkownika (GUI), kt\u00f3ry \u0142\u0105czy wszystkie metody w jedn\u0105 ca\u0142o\u015b\u0107 i pozwala na swobodn\u0105 (w pewnym zakresie) kombinacj\u0119 element\u00f3w. Nikt\u00f3re funkcje nie s\u0105 dost\u0119pne z poziomu GUI, kt\u00f3re nie maj\u0105 negatywnego wp\u0142ywu na samo dzia\u0142anie aplikacji, ale w przysz\u0142o\u015bci mo\u017cna je bez problemu doda\u0107. Okno aplikacji: ![Przyk\u0142adowe okno aplikacji](main_window.png)  Dokumentacja Dokumentacja najwa\u017cniejszych element\u00f3w projektu zosta\u0142a wykonana z pomoc\u0105 biblioteki pdoc3 (g\u0142\u00f3wnie w formacie html, dlatego najlepiej \"uruchomi\u0107\" j\u0105 u siebie lokalnie). Ponadto, z poziomu dokumentacji jest dost\u0119pna lokalna, funkcjonalna wyszukiwarka.  Konfiguracja (przy wykorzystaniu pojedynczych metod segmentacji) W poszczeg\u00f3lnych folderach danych metod segmentacji znajduj\u0105 si\u0119 pliki w formacie Markdown zawieraj\u0105ce opis ich konfiguracji.  Uruchamianie programu W celu poprawnego uruchomienia naszego programu konieczne jest skorzystanie z wirtualnego \u015brodowiska znajduj\u0105cego si\u0119 w repozytorium TrackingDevice. W tym celu potrzebny b\u0119dzie menad\u017cer pakiet\u00f3w conda.  $ conda activate ./env  Koniecznym mo\u017ce si\u0119 okaza\u0107 podanie pe\u0142nej scie\u017cki do \u015brodowiska. Po poprawnej aktywacji \u015brodowiska uruchamiamy program komend\u0105:  $ python gui.py   Zalecana konfiguracja: System operacyjny Windows 10 lub Linux, karta graficzna Nvidia (niewymagana).  [Raport dotycz\u0105cy ICNet oraz tracker\u00f3w dost\u0119pnych w bibliotece OpenCV](https: gitlab.com/slawomir.olszew/trackingdevice/-/blob/master/ICNet_tensorflow_master/README.md)  Raport por\u00f3wnawczy  Metody segmentacji Cecha |  MaskRCNN |  ICNet |  SiamMask  - |   |  - |  -  Dokladno\u015b\u0107 sieci(%) | 65.632 | 80.9 | 71.3  FPS przy segmentacji live | 0.419 | 0.992 | 100.5 - jeden obiekt 42.1 - dwa obiekty  Ilosc klas obiektow | 81 | 19 | 1000  Architektura | FPN + ResNet101 | ResNet50 z 3 pomocniczymi ga\u0142\u0119ziami | Siamese  Dataset | MS COCO | Cityscapes | ImageNet-1k Podsumowuj\u0105c, najlepszym wyborem jest metoda SiamMask. Pozwala ona na przetwarzanie obrazu live bez znacznego wp\u0142ywu na p\u0142ynno\u015b\u0107, posiada najwi\u0119ksz\u0105 ilo\u015b\u0107 rozpoznawanych klas, mo\u017cliwe jest \u015bledzenie wielu obiekt\u00f3w jednocze\u015bnie.  Metody \u015bledzenia (trackery) Cecha |  Optical Flow + Orb Descriptor |  OpenCV (CSRT Tracker) |  Dlib Tracker  - |  - |  -|  -  Ilo\u015b\u0107 FPS (\u015brednia) | 19.86 | 2.25 | 110.3  \u0179r\u00f3d\u0142o \u015bledzenia | Maska | Bounding box | Bounding box Z wynik\u00f3w oraz obserwacji samego dzia\u0142ania mo\u017cna wywnioskowa\u0107, \u017ce najlepszym trackerem (metod\u0105 \u015bledzenia) jest ten z biblioteki Dlib. Cechuje si\u0119 on du\u017c\u0105 szybko\u015bci\u0105 dzia\u0142ania i samo \u015bledzenie r\u00f3wnie\u017c jest dok\u0142adne (bouding box jest zawsze w obr\u0119bie \u015bledzonego obiektu i nie \"lata\" po ekranie). Wybrany tracker z biblioteki OpenCV jest niestety zbyt wolny dla wygodnego u\u017cytkowania. Metoda \u0142\u0105cz\u0105ca Optical Flow oraz Orb Descriptor, mimo po\u017c\u0105danego dzia\u0142ania (wykorzystanie maski z segmentacji semantycznej) oraz akceptowalnej szybko\u015bci dzia\u0142ania, nie jest jednak tak dok\u0142adny, jakby tego oczekiwano (popraw\u0119 mo\u017cnaby uzyska\u0107 doborem innych parametr\u00f3w i/lub ci\u0105g\u0142\u0105 segmentacj\u0105 (aktualna maska), co jednak mog\u0142oby mie\u0107 do\u015b\u0107 negatywny wp\u0142yw na szybko\u015b\u0107 dzia\u0142ania). Poza opisanymi i zaimplementowanymi trackerami jest r\u00f3wnie\u017c jeden wbudowany w metod\u0119 segmentacji SiamMask, kt\u00f3rego szybko\u015b\u0107 mo\u017cna zakwalifikowa\u0107 do szybko\u015bci samej segmentacja, kt\u00f3ra jest zadowalaj\u0105ca oraz bardzo dok\u0142adna."
},
{
"ref":"gui",
"url":1,
"doc":" Main module responsible for TrackingDevice GUI and combining all other modules used for segmentation and tracking."
},
{
"ref":"gui.Glowne",
"url":1,
"doc":" GUI class, derives from QMainWindow class. Attributes      segmentFuncSet : list List of available segmentation methods (networks). trackersSet : list List of available trackers. chosenSegmentFunc : str Default segmentation method. chosenTracker : str Default tracker. startButton : QPushButton Button that starts process/simulation. funcDropdown : QComboBox Dropdown box where segmentation method can be selected. trackerDropdown : QComboBox Dropdown box where tracker can be selected. imageLabel : QLabel Label that contains image which was selected for segmentation. processedImageLabel : QLabel Label that contains text about processing video/image and if camera is chosen instead of video file. pickFileButton : QPushButton Button that opens file picker to choose video/image file. filePathLabel : QLabel Label that shows file path and if any file has been selected. width : int Window width. height : int Window height. filePath : str Path to video/image file. imageChosen : bool True if image file was chosen. videoChosen : bool True if video file was chosen. imageName : None Unused variable.  Attributes outside __init__ ( Glowne.initUI ) ifcam : QCheckBox When checked, video source will be from camera instead of video file (SiamMask). button_stop : QPushButton Button that allows to start/stop/resume video file (SiamMask). button_close : QPushButton Button that closes SiamMask window. button_select : QPushButton Button that allows to select object on image/video (SiamMask). Parameters      w : int Primary screen width. h : int Primary screen height."
},
{
"ref":"gui.Glowne.perToPix",
"url":1,
"doc":"Get pixel position on window along given axis and percentage. Parameters      percent : int Percentage value. axis : str 'w' if width, 'h' if height. Returns    - int Value along given axis and percentage.",
"func":1
},
{
"ref":"gui.Glowne.resizeEvent",
"url":1,
"doc":"Overridden method, resize window based on given frame geometry, called by  setGeometry() in  Glowne.initUI . Parameters      event : QResizeEvent Event that occurs when window is resized (programatically) Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.update",
"url":1,
"doc":"Update all GUI elements (buttons, labels etc.) in terms of position, size and visibility. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.initUI",
"url":1,
"doc":"Initialize GUI elements (connect elements to certain actions (functions), set window styles, size and title), declare elements: ifcam, button_stop, button_close, button_select. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.on_stop_clicked",
"url":1,
"doc":"Start/stop/resume video when SiamMask method is active. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.on_close_clicked",
"url":1,
"doc":"Close SiamMask window and change visibility of its GUI elements. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.on_select_clicked",
"url":1,
"doc":"Select new SiamMask object, preceeded by proper QMessageBox. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.processRCNNImage",
"url":1,
"doc":"Process image when using MaskRCNN method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.processSIAMImage",
"url":1,
"doc":"Process image when using SiamMask method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.processICNETImage",
"url":1,
"doc":"Process image when using ICNET method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.processSIAMVideo",
"url":1,
"doc":"Process video when using SiamMask method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.processICNETVideo",
"url":1,
"doc":"Process image when using ICNET method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.processRCNNVideo",
"url":1,
"doc":"Process image when using MaskRCNN method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.startProcessing",
"url":1,
"doc":"Start processing image/video based on chosen elements. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.changeFunc",
"url":1,
"doc":"Change/update all necessary elements when segmentation method is chosen from dropdown menu. Parameters      chosen : str Chosen segmentation method. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.changeTracker",
"url":1,
"doc":"Change/update all necessary elements when tracker is chosen from dropdown menu. Parameters      chosen : str Chosen tracker. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.setNewProcessedImage",
"url":1,
"doc":"Change/update all necessary elements when function is called (show processed image in GUI). Parameters      imgPath : str Path/name of the image. Returns    - None",
"func":1
},
{
"ref":"gui.Glowne.openFiles",
"url":1,
"doc":"Handle file selection when  Glowne.pickFileButton is clicked. Returns    - None",
"func":1
},
{
"ref":"mask_r_cnn.rcnn",
"url":2,
"doc":" Segmentation module using [Mask R-CNN](https: github.com/matterport/Mask_RCNN)."
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig",
"url":2,
"doc":"Configuration for training on MS COCO. Derives from the base Config class and overrides values specific to the COCO dataset. Set values of computed attributes."
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.GPU_COUNT",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.IMAGES_PER_GPU",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.MAX_GT_INSTANCES",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.TRAIN_ROIS_PER_IMAGE",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.RPN_ANCHOR_STRIDE",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.POST_NMS_ROIS_TRAINING",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.POST_NMS_ROIS_INFERENCE",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.IMAGE_MIN_DIM",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.IMAGE_MAX_DIM",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.InferenceConfig.DETECTION_MAX_INSTANCES",
"url":2,
"doc":""
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN",
"url":2,
"doc":" MaskRCNN class, contains all necessary methods. Attributes      CURRENT_PATH : pathlib.Path Path to this file adequate to used operating system. MODEL_DIR : str Path to model directory (output). COCO_MODEL_PATH : str Path to coco model in h5 format. scaleFactor : float Variable used to scale frame (image).  Attributes outside __init__ ( MaskRCNN.set_configuration ) config  InferenceConfig object"
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.initialize",
"url":2,
"doc":"Set MaskRCNN configuration and load coco model. Returns    - None",
"func":1
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.set_configuration",
"url":2,
"doc":"Configure training parameters using  InferenceConfig . Returns    - None",
"func":1
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.load_model",
"url":2,
"doc":"Download COCO trained weights (if model can't be found) and load model (weights). Returns    - None",
"func":1
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.scaleFrame",
"url":2,
"doc":"Resize frame using  cv2.resize . Parameters      frame : numpy.ndarray cv2 frame. Returns    - copiedFrame : numpy.ndarray Resized frame.",
"func":1
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.processFrame",
"url":2,
"doc":"Process frame (detect objects), alternative to  MaskRCNN.get_masked_frame . Not used. Parameters      frame : numpy.ndarray cv2 frame. Returns    - upScaledRois : list(list(int Detected, upscaled ROIs. r['class_ids'] : list(int) Detected objects ids. names : list(str) Detected object names (COCO).",
"func":1
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.showFrame",
"url":2,
"doc":"Show frame and draw bouding boxes on it. Not used. Parameters      frame : numpy.ndarray cv2 frame. boxes : list(list(int Bounding boxes coordinates",
"func":1
},
{
"ref":"mask_r_cnn.rcnn.MaskRCNN.get_masked_frame",
"url":2,
"doc":"Main MaskRCNN function which detects objects on frame and returns results in proper form. Parameters      frame : numpy.ndarray cv2 frame. Returns    - masked_image : numpy.ndarray Image (frame) with applied masks of detected objects. frame_masks: list(numpy.ndarray) Detected objects masks. totalTime : float Total time which was taken to detect objects on frame.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask",
"url":3,
"doc":" Segmentation and tracking module using [SiamMask - Fast Online Object Tracking and Segmentation](https: github.com/foolwood/SiamMask)."
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask",
"url":3,
"doc":" SiamMask class traces one object. Parameters      siammask Initialized siammask instance. cfg Siammask configuration. device Torch device. mask_colour : list(int) Colour of mask (as [R, G, B] with intensity form 0 to 255). bbox_colour : list(int) Colour of bounding box (as [R, G, B] with intensity form 0 to 255). mask_opacity : float Mask opacity, 1.0 is full color 0.0 is transparent. save_first_frame : bool If true saves first processed frame as png. Attributes      siammask Initialized siammask instance. cfg Siammask configuration. device Torch device. f : int Helper variable, if equals 0, process frame as inital, if greater than 0, process frame \"normally\". state : dict Contains all the necessary information (state) about Siammask object (mask, masked object location etc.). mask_colour : list(int) Colour of mask (as [R, G, B] with intensity form 0 to 255). bbox_colour : list(int) Colour of bounding box (as [R, G, B] with intensity form 0 to 255). mask_opacity : float Mask opacity, 1.0 is full color 0.0 is transparent. save_first_frame : bool If true saves first processed frame as png."
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.set_target_pos",
"url":3,
"doc":"Set target rectangle positon and dimensions. Parameters      init_rect : tuple Initial rectangle boundingbox as Tuple. Returns    - None",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.process",
"url":3,
"doc":"Main SiamMask process. Parameters      frame : numpy.ndarray cv2 frame. Returns    - bool True if processing was successful. frame : numpy.ndarray cv2 output frame.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.draw_on_frame",
"url":3,
"doc":"Draw mask and boundingbox on given frame in given location. Parameters      frame : numpy.ndarray cv2 frame. location : numpy.ndarray Location of object. mask : numpy.ndarray Boolean mask of an object, True where object is located (frame size). Returns    - frame : numpy.ndarray cv2 output frame.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.process_first_frame",
"url":3,
"doc":"Initialize first frame process. Parameters      frame : numpy.ndarray CV2 frame. Returns    - dict Contains all the necessary information (state) about Siammask object (mask, masked object location etc.).",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.old_process_frame",
"url":3,
"doc":"Process siammask tracing on new frame, updates state. Parameters      frame : numpy.ndarray cv2 frame. Returns    - state : dict Contains all the necessary information (state) about Siammask object (mask, masked object location etc.).",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.process_frame",
"url":3,
"doc":"Process siammask tracing on new frame, updates state. Parameters      frame : numpy.ndarray cv2 frame. Returns    - state : dict Contains all the necessary information (state) about Siammask object (mask, masked object location etc.). location : numpy.ndarray Location of traced object. mask : numpy.ndarray Boolean mask of an object, True where object is located (frame size).",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.load_model",
"url":3,
"doc":"Unused, empty function.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamMask.get_mask",
"url":3,
"doc":"Return boolean array with size of frame. Parameters      frame : numpy.ndarray cv2 frame. Returns    - mask : numpy.ndarray Boolean mask of an object, True where object is located (frame size). None If mask can't be found.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling",
"url":3,
"doc":" Handles Siammask tracing (with multiple objects). Attributes      args Argparse namespace object. path : str Path to video file. window_name : str CV2 window name. objects : list List of SiamMask objects. start : bool Allows to start/resume video. stop : bool Allows to start/resume video."
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.toggle_play",
"url":3,
"doc":"Toggle visualisation play. Returns    - None",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.new_select",
"url":3,
"doc":"Select new ROI. Returns    - None",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.setup_model",
"url":3,
"doc":"Setup model and return siammask model, cfg and device. Parameters      args Argparse namespace object. Returns    - siammask Siammask model. cfg Siammask configuration. device Torch device.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.video",
"url":3,
"doc":"Return path VideoCapture. Parameters      path : str Path to video file. Returns    - cv2.VideoCapture",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.camera",
"url":3,
"doc":"Select and return live camera VideoCapture. Returns    - cv2.VideoCapture",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.process_one_frame",
"url":3,
"doc":"Process only one frame (image) and shows / saves result. Parameters      args Argparse namespace object. Returns    - None",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.initialize_and_run",
"url":3,
"doc":"Main handling function/loop. Parameters      args Argparse namespace object. Returns    - None",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.print_info",
"url":3,
"doc":"Print keyboard keys info for user. Parameters      args Argparse namespace object. Returns    - None",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.capture_frame",
"url":3,
"doc":"Capture current VideoCapture frame. Returns    - ret : bool If True frame was captured successfully. frame : numpy.ndarray cv2 frame.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.select_ROI",
"url":3,
"doc":"Select ROI using cv2 method. Opens interface that allow to select traced object ROI. Parameters      frame : numpy.ndarray cv2 frame. siam_mask SiamMask class instance. Returns    - bool True if selection was successful.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.SiamHandling.select_new_ROI",
"url":3,
"doc":"Create new SiamMask object and initialize selection. Parameters      frame : numpy.ndarray cv2 frame stop : bool True if play is paused. args Argparse namespace object. Returns    - bool True if selection was successful.",
"func":1
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.Args",
"url":3,
"doc":" Wrapper around argparse which allow to use siammask in GUI. Attributes      parser Argparse object. Parameters      filePath : str Video file path. imageChosen : bool If True saves processed image. davisPthPath : str Path to pretrained model. davisJsonPath : str Path to model configuration (in JSON)."
},
{
"ref":"SiamMask.experiments.siammask_sharp.siam_mask.Args.parse_args",
"url":3,
"doc":"Parse arguments in array to argparse. Parameters      list_of_args : list Array of arguments. Returns    - args Argparse namespace object.",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet",
"url":4,
"doc":" Segmentation module using [ICNet - tensorflow implementation](https: github.com/hellochick/ICNet-tensorflow)."
},
{
"ref":"ICNet_tensorflow_master.ICNet.Configuration",
"url":4,
"doc":" Configuration class, derivates from custom ICNet config structure. Attributes      m_type : str Key of model to use (ICNet). m_weight : str Path to model file. IMG_SIZE : tuple(int) Image dimensions."
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass",
"url":4,
"doc":" ICNet class, contains all necessary methods. Attributes      modelPath : str Path to model in .npy format. m_configuration : dict Data structure with available ICNet options (models), currently, 'trainval' is used. data_set : str Chosen data set, currently, 'cityscapes' is used. filter_scale : int Variable used in ICNet configuration. args Argparse namespace object. vid : bool Set to true (video processing). Not used.  Attributes outside __init__ ( IcnetClass.create_session ) conf  Configuration object. model Model selected from  IcnetClass.m_configuration . net Initialized ICNet model."
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.initialize_model",
"url":4,
"doc":"Set ICNet configuration and create session (initialize model). Returns    - None",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.set_config",
"url":4,
"doc":"Set ICNet configuration with user-set parameters. Returns    - None",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.create_session",
"url":4,
"doc":"Initialize and create session of selected ICNet model. Returns    - None",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.scale_image",
"url":4,
"doc":"Resize frame using  cv2.resize with  Configuration.IMG_SIZE dimensions. Parameters      frame : numpy.ndarray cv2 frame. Returns    - scaled_img : numpy.ndarray Resized frame.",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.processing_img",
"url":4,
"doc":"Process frame (image) with ICNet model and calculate processing time. Used when ICNet is used separately (standalone). Parameters      frame : numpy.ndarray cv2 frame. Returns    - out : numpy.ndarray Output frame (with masks). duration : float Time which was taken to process (predict) objects on frame with ICNet model. mask : numpy.ndarray Mask (masks) of detected object (objects).",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.get_masks",
"url":4,
"doc":"Main function, process video frame with ICNet model. Parameters      frame : numpy.ndarray cv2 frame. Returns    - out : numpy.ndarray Output frame (with masks). mask : numpy.ndarray Mask (masks) of detected object (objects). Notes   - Unfortunately, images (single frame) can't be processed in GUI with ICNet.",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.image_handling",
"url":4,
"doc":"Function used to handle frame (image) processing if ICNet is used separately (standalone). Shows processing time. Parameters      frame : numpy.ndarray cv2 frame. Returns    - None",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.processing_vid",
"url":4,
"doc":"Function used to process single video frame if ICNet is used separately (standalone). Parameters      frame : numpy.ndarray cv2 frame. Returns    - out : numpy.ndarray Output frame (with masks). duration : float Time which was taken to process (predict) objects on frame with ICNet model. mask : numpy.ndarray Mask (masks) of detected object (objects).",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.IcnetClass.video_handling",
"url":4,
"doc":"Function used to handle video processing if ICNet is used separately (standalone). Shows processing time. Parameters      frame Path to video file or camera selection (int). Returns    - None",
"func":1
},
{
"ref":"ICNet_tensorflow_master.ICNet.createICNETInstance",
"url":4,
"doc":"Function responsible for creating ICNet instance (configuration, initialization) and getting its results ( IcnetClass.get_masks ). Used in [GUI](gui.html). Parameters      modelPath : numpy.ndarray cv2 frame. filePath : str Path to image or video. videoChosen : numpy.ndarray Frame of the video. Returns    - out : numpy.ndarray Output frame with masks. mask : numpy.ndarray Array containing mask (masks).",
"func":1
},
{
"ref":"tracker",
"url":5,
"doc":" Tracker module using [OpenCV MultiTracker](https: docs.opencv.org/4.4.0/d8/d77/classcv_1_1MultiTracker.html). Can be used with [MaskRCNN](mask_r_cnn.rcnn.html) and [ICNet](ICNet_tensorflow_master.ICNet.html)"
},
{
"ref":"tracker.Tracker",
"url":5,
"doc":" cv2 tracker class. Attributes      bounding_box : tuple Tuple that contains bounding box coordinates (x1, y1, x2, y2). tracker : cv2.MultiTracker cv2 MultiTracker object. Notes   - Currently,  cv2.TrackerCSRT tracker is used with  cv2.MultiTracker and therefore in project."
},
{
"ref":"tracker.Tracker.start",
"url":5,
"doc":"Main tracker function, called every read frame. Draw bounding box and arrows (if object is near edges) on frame. Parameters      frame : numpy.ndarray cv2 frame. output: numpy.ndarray cv2 output frame. Returns    - None",
"func":1
},
{
"ref":"tracker.Tracker.bbox_choice",
"url":5,
"doc":"Choose bounding box using  cv2.selectROI() and add  cv2.TrackerCSRT to  cv2.MultiTracker . Not used. Parameters      output: numpy.ndarray cv2 output frame. Returns    - None",
"func":1
},
{
"ref":"tracker.Tracker.new_bbox",
"url":5,
"doc":"Choose new bounding box using  cv2.selectROI() and add  cv2.TrackerCSRT to  cv2.MultiTracker . Reset bounding_box attribute first. Not used. Parameters      output: numpy.ndarray cv2 output frame. Returns    - None",
"func":1
},
{
"ref":"tracker_dlib",
"url":6,
"doc":" Tracker module using [tracker from dlib library](http: dlib.net/python/index.html dlib.correlation_tracker). Can be used with [MaskRCNN](mask_r_cnn.rcnn.html) and [ICNet](ICNet_tensorflow_master.ICNet.html)"
},
{
"ref":"tracker_dlib.Tracker",
"url":6,
"doc":" Dlib tracker class. Attributes      bounding_box : tuple Tuple that contains bounding box coordinates (x1, y1, x2, y2). tracker : dlib.correlation_tracker Dlib tracker object."
},
{
"ref":"tracker_dlib.Tracker.start",
"url":6,
"doc":"Main tracker function, called every read frame. Draw bounding box and arrows (if object is near edges) on frame. Parameters      frame : numpy.ndarray cv2 frame. output: numpy.ndarray cv2 output frame. Returns    - None",
"func":1
},
{
"ref":"tracker_dlib.Tracker.bbox_choice",
"url":6,
"doc":"Choose bounding box using  cv2.selectROI() and start tracking. Not used. Parameters      output: numpy.ndarray cv2 output frame. Returns    - None",
"func":1
},
{
"ref":"tracker_dlib.Tracker.new_bbox",
"url":6,
"doc":"Choose new bounding box using  cv2.selectROI() and start tracking. Reset bounding_box attribute first. Not used. Parameters      output: numpy.ndarray cv2 output frame. Returns    - None",
"func":1
},
{
"ref":"orb_tracker",
"url":7,
"doc":" Tracker module realizing optical flow + orb descriptor method. Can be used with [MaskRCNN](mask_r_cnn.rcnn.html) and [ICNet](ICNet_tensorflow_master.ICNet.html)"
},
{
"ref":"orb_tracker.orb",
"url":7,
"doc":"Main function of optical flow + orb descriptor tracking method. Uses [OpenCV ORB](https: docs.opencv.org/4.4.0/d1/d89/tutorial_py_orb.html) descriptor and [OpenCV Optical Flow](https: docs.opencv.org/4.4.0/d4/dee/tutorial_optical_flow.html) implementations. Parameters      cap : cv2.VideoCapture Object used for video capturing from video files, image sequences or cameras. mask : numpy.ndarray Mask of an object to track/tracked object. old_frame: numpy.ndarray Initial cv2 frame. Returns    - None",
"func":1
}
]
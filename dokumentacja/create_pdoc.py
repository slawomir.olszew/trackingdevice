import pdoc
import os
import sys
from modify_html import modify_html

# komenda do wygenerowania plików potrzebnych do lunr_search (z poziomu folderu trackingdevice):
# pdoc dokumentacja.__init__ gui mask_r_cnn.rcnn SiamMask.experiments.siammask_sharp.siam_mask ICNet_tensorflow_master.ICNet tracker tracker_dlib orb_tracker --html -o dokumentacja -c lunr_search="{'fuzziness': 1, 'index_docstrings': True}" -f
# może być problem z plikami nieznajdującymi się w tym samym folderze (segmentacja), póki co trzeba ręcznie poprawiać html oraz index.js

def recursive_htmls(mod):
    yield mod.name, mod.html()
    for submod in mod.submodules():
        yield from recursive_htmls(submod)

def create_pdoc():
    pdoc.tpl_lookup.directories.insert(0, ".")

    sys.path.insert(0, os.path.abspath(".."))

    files = ['dokumentacja.__init__', 'gui', 'mask_r_cnn.rcnn', 'SiamMask.experiments.siammask_sharp.siam_mask', 'ICNet_tensorflow_master.ICNet', 'tracker', 'tracker_dlib', 'orb_tracker']
    modules = [pdoc.Module(mod) for mod in files]
    pdoc.link_inheritance()

    for mod in modules:
        for module_name, html in recursive_htmls(mod):
            with open(module_name + ".html", "w") as file:
                file.write(html)

    modify_html()

if __name__ == "__main__":
    create_pdoc()
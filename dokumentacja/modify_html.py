import os
from bs4 import BeautifulSoup

def rename(file_list, name):
    rename_list = ["Dokumentacja - README", "GUI", "ICNet", "Mask R-CNN", "ORB Descriptor + Optical Flow Tracker", "SiamMask", "CV2 Tracker", "Dlib Tracker"]
    for original, renamed in zip(file_list, rename_list):
        if name == original:
            return renamed

def modify_html():
    file_list = [item for item in os.listdir(".") if item.endswith(".html") and item != "doc-search.html"]
    # ['dokumentacja.html', 'gui.html', 'ICNet_tensorflow_master.ICNet.html', 'mask_r_cnn.rcnn.html', 'orb_tracker.html', 'SiamMask.experiments.siammask_sharp.siam_mask.html', 'tracker.html', 'tracker_dlib.html']

    for item in file_list:
        added_files= []
        with open(item, 'r') as file:
            html_data = file.read()
            soup_data = BeautifulSoup(html_data, 'html.parser')
        
        for element in soup_data.find_all("div", attrs={"class": "toc"}):
            for file_name in file_list:
                if file_name not in added_files:
                    temp_string = '<ul><li><a href="{}">{}</a></li></ul>'.format(file_name, rename(file_list, file_name))
                    new_data = BeautifulSoup(temp_string, 'html.parser')
                    element.insert(-1, new_data)
                    added_files.append(file_name)
        
        with open(item, "w") as file:
            file.write(str(soup_data))

if __name__ == "__main__":
    modify_html()
    print("Modified html")
# Dokumentacja projektu [Tracking Device](https://gitlab.com/slawomir.olszew/trackingdevice)
### Autorzy: [Sławomir Olszewski](https://gitlab.com/slawomir.olszew) [Mykyta Brazhynskyy](https://gitlab.com/mykyta.brazhynskyy) [Krzysztof Jeschke](https://gitlab.com/jeschke.krzysztof44) [Rafał Kaliszak](https://gitlab.com/rafalkaliszak) [Piotr Truty](https://gitlab.com/P.Truty) [Marta Gozdowska](https://gitlab.com/margoo9) [Marek Mikulski](https://gitlab.com/marek_mikulski) [Maciej Kuraż](https://gitlab.com/MKuraz) [Sebastian Lewalski](https://gitlab.com/LewalskiSebastian) [Dariusz Libecki](https://gitlab.com/dariuszlibecki) [Paweł Niedziela](https://gitlab.com/pawel.n)

TrackingDevice to projekt mający na celu opracowanie metod śledzenia - segmentację semantyczną obrazu (klatki filmu) a następnie śledzenie wybranego obiektu lub obiektów. W tym celu została stworzona aplikacja desktopowa w języku programowania Python.
Można zatem podzielić projekt na dwie części, gdzie zdecydowano się na zaimplementowanie kilku metod (arbitralnie uznanych za sensowne i skuteczne) dla każdej z nich:

1. segmentacja obrazu
    - [Mask R-CNN](https://github.com/matterport/Mask_RCNN)
    - [ICNet](https://github.com/hellochick/ICNet-tensorflow)
    - [SiamMask](https://github.com/foolwood/SiamMask)
2. śledzenie obiektu/obiektów (trackery)
    - [optical flow](https://docs.opencv.org/4.4.0/d4/dee/tutorial_optical_flow.html) + [orb descriptor](https://docs.opencv.org/4.4.0/d1/d89/tutorial_py_orb.html)
    - [tracker z biblioteki Dlib (correlation tracker)](http://dlib.net/python/index.html#dlib.correlation_tracker)
    - [trackery z biblioteki OpenCV](https://docs.opencv.org/4.4.0/d9/df8/group__tracking.html) (wybrany - [CSRT tracker](https://docs.opencv.org/4.4.0/d2/da2/classcv_1_1TrackerCSRT.html))

## GUI
Centrum projektu jest graficzny interfejs użytkownika (GUI), który łączy wszystkie metody w jedną całość i pozwala na swobodną (w pewnym zakresie) kombinację elementów. Niktóre funkcje nie są dostępne z poziomu GUI, które nie mają negatywnego wpływu na samo działanie aplikacji, ale w przyszłości można je bez problemu dodać.

Okno aplikacji:
![Przykładowe okno aplikacji](main_window.png)

## Dokumentacja
Dokumentacja najważniejszych elementów projektu została wykonana z pomocą biblioteki pdoc3 (głównie w formacie html, dlatego najlepiej "uruchomić" ją u siebie lokalnie). Ponadto, z poziomu dokumentacji jest dostępna lokalna, funkcjonalna wyszukiwarka.

## Konfiguracja (przy wykorzystaniu pojedynczych metod segmentacji)
W poszczególnych folderach danych metod segmentacji znajdują się pliki w formacie Markdown zawierające opis ich konfiguracji.

## Uruchamianie programu
W celu poprawnego uruchomienia naszego programu konieczne jest skorzystanie z wirtualnego środowiska znajdującego się w repozytorium TrackingDevice. W tym celu potrzebny będzie menadżer pakietów conda.
    ```
    $ conda activate ./env
    ```

Koniecznym może się okazać podanie pełnej scieżki do środowiska.
Po poprawnej aktywacji środowiska uruchamiamy program komendą:
    ```
    $ python gui.py
    ```

### Zalecana konfiguracja: System operacyjny Windows 10 lub Linux, karta graficzna Nvidia (niewymagana).

## [Raport dotyczący ICNet oraz trackerów dostępnych w bibliotece OpenCV](https://gitlab.com/slawomir.olszew/trackingdevice/-/blob/master/ICNet_tensorflow_master/README.md)

## Raport porównawczy

### Metody segmentacji 
Cecha | **MaskRCNN** | **ICNet** | **SiamMask**
--- | ---- | --- | ---
**Dokladność sieci(%)** | 65.632 | 80.9 | 71.3
**FPS przy segmentacji live** | 0.419 | 0.992 | 100.5 - jeden obiekt<br> 42.1 - dwa obiekty
**Ilosc klas obiektow** | 81 | 19 | 1000
**Architektura** | FPN + ResNet101 | ResNet50 z 3 pomocniczymi gałęziami | Siamese
**Dataset** | MS COCO | Cityscapes | ImageNet-1k

Podsumowując, najlepszym wyborem jest metoda SiamMask. Pozwala ona na przetwarzanie obrazu live
bez znacznego wpływu na płynność, posiada największą ilość rozpoznawanych klas, możliwe jest śledzenie
wielu obiektów jednocześnie.

### Metody śledzenia (trackery) 
Cecha | **Optical Flow + Orb Descriptor** | **OpenCV (CSRT Tracker)** | **Dlib Tracker**
--- | --- | ---| ---
**Ilość FPS (średnia)** | 19.86 | 2.25 | 110.3
**Źródło śledzenia** | Maska | Bounding box | Bounding box

Z wyników oraz obserwacji samego działania można wywnioskować, że najlepszym trackerem (metodą śledzenia) jest
ten z biblioteki Dlib. Cechuje się on dużą szybkością działania i samo śledzenie również jest dokładne (bouding box jest zawsze w obrębie śledzonego obiektu i nie "lata" po ekranie). Wybrany tracker z biblioteki OpenCV jest niestety zbyt wolny dla wygodnego
użytkowania. Metoda łącząca Optical Flow oraz Orb Descriptor, mimo pożądanego działania (wykorzystanie maski z segmentacji semantycznej) oraz akceptowalnej szybkości działania, nie jest jednak tak dokładny, jakby tego oczekiwano (poprawę możnaby uzyskać doborem innych parametrów i/lub ciągłą segmentacją (aktualna maska), co jednak mogłoby mieć dość negatywny wpływ na szybkość działania).
Poza opisanymi i zaimplementowanymi trackerami jest również jeden wbudowany w metodę segmentacji SiamMask, którego szybkość można zakwalifikować do szybkości samej segmentacja, która jest zadowalająca oraz bardzo dokładna.
"""
### Main module responsible for TrackingDevice GUI and combining all other modules used for segmentation and tracking.
"""
from PyQt5.QtWidgets import QWidget, QPushButton, QApplication, QLineEdit, QMessageBox, QLabel, QVBoxLayout, \
    QListWidget, QComboBox, QListWidgetItem, QTableWidget, QTableWidgetItem, QGridLayout, QHeaderView, QMainWindow, \
    QFileDialog, QRadioButton, QCheckBox
from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import QPalette, QPixmap, QImage
import sys
from pathlib import Path
import os
import tensorflow as tf

tf.disable_v2_behavior()
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
from mask_r_cnn.rcnn import MaskRCNN
import cv2
import dlib
import time
from SiamMask.experiments.siammask_sharp.siam_mask import SiamHandling, Args
from ICNet_tensorflow_master.ICNet import createICNETInstance
import numpy as np
from orb_tracker import orb
from tracker_dlib import Tracker as DlibTracker
from tracker import Tracker as CVTracker


class Glowne(QMainWindow):
    """
    ## GUI class, derives from QMainWindow class.

    Attributes
    ----------
    segmentFuncSet : list
        List of available segmentation methods (networks).
    trackersSet : list
        List of available trackers.
    chosenSegmentFunc : str
        Default segmentation method.
    chosenTracker : str
        Default tracker.
    startButton : QPushButton
        Button that starts process/simulation.
    funcDropdown : QComboBox
        Dropdown box where segmentation method can be selected.
    trackerDropdown : QComboBox
        Dropdown box where tracker can be selected.
    imageLabel : QLabel
        Label that contains image which was selected for segmentation.
    processedImageLabel : QLabel
        Label that contains text about processing video/image and if camera is chosen instead of video file.
    pickFileButton : QPushButton
        Button that opens file picker to choose video/image file.
    filePathLabel : QLabel
        Label that shows file path and if any file has been selected.
    width : int
        Window width.
    height : int
        Window height.
    filePath : str
        Path to video/image file.
    imageChosen : bool
        True if image file was chosen.
    videoChosen : bool
        True if video file was chosen.
    imageName : None
        Unused variable.
    ## Attributes outside __init__ (`Glowne.initUI`)
    ifcam : QCheckBox
        When checked, video source will be from camera instead of video file (SiamMask).
    button_stop : QPushButton
        Button that allows to start/stop/resume video file (SiamMask).
    button_close : QPushButton
        Button that closes SiamMask window.
    button_select : QPushButton
        Button that allows to select object on image/video (SiamMask).

    Parameters
    ----------
    w : int
        Primary screen width.
    h : int
        Primary screen height.
    """
    def __init__(self, w, h):
        super().__init__()
        self.segmentFuncSet = ['RCNN', 'SIAM', 'ICNET']
        self.trackersSet = ['ORB', 'DLIB', 'CV2']
        self.chosenSegmentFunc = 'RCNN'
        self.chosenTracker = 'ORB'
        self.startButton = QPushButton('START', self)
        self.funcDropdown = QComboBox(self)
        self.trackerDropdown = QComboBox(self)
        for tracker in self.trackersSet:
            self.trackerDropdown.addItem(tracker)
        for item in self.segmentFuncSet:
            self.funcDropdown.addItem(item)
        self.imageLabel = QLabel(self)
        self.processedImageLabel = QLabel(self)
        self.pickFileButton = QPushButton('Wybierz plik', self)
        self.filePathLabel = QLabel(self)
        self.filePathLabel.setText('Brak wybranego pliku')
        self.width = w
        self.height = h
        self.filePath = None
        self.imageChosen = False
        self.videoChosen = False
        self.imageName = None
        self.initUI()

    def perToPix(self, percent, axis):
        """
        Get pixel position on window along given axis and percentage.

        Parameters
        ----------
        percent : int
            Percentage value.
        axis : str
            'w' if width, 'h' if height.

        Returns
        -------
        int
            Value along given axis and percentage.
        """
        if axis == 'w':
            return percent * self.width / 100
        else:
            return percent * self.height / 100

    def resizeEvent(self, event):
        """
        Overridden method, resize window based on given frame geometry, called by `setGeometry()` in `Glowne.initUI`.

        Parameters
        ----------
        event : QResizeEvent
            Event that occurs when window is resized (programatically)

        Returns
        -------
        None
        """
        self.width = self.frameGeometry().width()
        self.height = self.frameGeometry().height()
        self.update()

    def update(self):
        """
        Update all GUI elements (buttons, labels etc.) in terms of position, size and visibility.

        Returns
        -------
        None
        """
        self.ifcam.move(self.perToPix(70, 'w'), self.perToPix(15, 'h'))
        self.button_stop.move(self.perToPix(85, 'w'), self.perToPix(15, 'h'))
        self.button_close.move(self.perToPix(70, 'w'), self.perToPix(10, 'h'))
        self.button_select.move(self.perToPix(85, 'w'), self.perToPix(10, 'h'))
        if self.chosenSegmentFunc == 'SIAM':
            self.ifcam.setVisible(True)
        else:
            self.ifcam.setVisible(False)
            self.button_stop.setVisible(False)
            self.button_close.setVisible(False)
            self.button_select.setVisible(False)

        self.pickFileButton.resize(self.perToPix(20, 'w'), self.perToPix(10, 'h'))
        self.pickFileButton.move(self.perToPix(70, 'w'), self.perToPix(20, 'h'))

        self.imageLabel.move(self.perToPix(2, 'w'), self.perToPix(2, 'h'))
        self.imageLabel.resize(self.perToPix(50, 'w'), self.perToPix(40, 'h'))

        self.processedImageLabel.move(self.perToPix(2, 'w'), self.perToPix(52, 'h'))
        self.processedImageLabel.resize(self.perToPix(50, 'w'), self.perToPix(40, 'h'))

        self.funcDropdown.resize(self.perToPix(20, 'w'), self.perToPix(10, 'h'))
        self.funcDropdown.move(self.perToPix(70, 'w'), self.perToPix(35, 'h'))

        self.trackerDropdown.resize(self.perToPix(20, 'w'), self.perToPix(10, 'h'))
        self.trackerDropdown.move(self.perToPix(70, 'w'), self.perToPix(45, 'h'))

        self.startButton.resize(self.perToPix(20, 'w'), self.perToPix(10, 'h'))
        self.startButton.move(self.perToPix(70, 'w'), self.perToPix(60, 'h'))

        self.filePathLabel.resize(self.perToPix(40, 'w'), self.perToPix(10, 'h'))
        self.filePathLabel.move(self.perToPix(60, 'w'), self.perToPix(80, 'h'))

    def initUI(self):
        """
        Initialize GUI elements (connect elements to certain actions (functions), set window styles, size and title), 
        declare elements: ifcam, button_stop, button_close, button_select.

        Returns
        -------
        None
        """
        button_style = QtGui.QFont("Times", 15, QtGui.QFont.Bold)
        self.pickFileButton.clicked.connect(self.openFiles)
        self.pickFileButton.setFont(button_style)

        self.funcDropdown.setStyleSheet("background-color : blue")
        self.funcDropdown.activated.connect(self.changeFunc)

        self.trackerDropdown.setStyleSheet("background-color : blue")
        self.trackerDropdown.activated.connect(self.changeTracker)

        self.startButton.clicked.connect(self.startProcessing)
        self.startButton.setFont(button_style)

        self.ifcam = QCheckBox("Camera", self)
        self.button_stop = QPushButton("Start/Stop video", self)
        self.button_close = QPushButton("Close SiamMask", self)
        self.button_select = QPushButton("Select object", self)

        self.button_stop.clicked.connect(self.on_stop_clicked)
        self.button_close.clicked.connect(self.on_close_clicked)
        self.button_select.clicked.connect(self.on_select_clicked)
        self.setGeometry(300, 200, 900, 800)
        self.setWindowTitle('Główne okno')
        self.update()
        self.show()

    def on_stop_clicked(self):
        """
        Start/stop/resume video when SiamMask method is active.

        Returns
        -------
        None
        """
        self.handling.toggle_play()

    def on_close_clicked(self):
        """
        Close SiamMask window and change visibility of its GUI elements.

        Returns
        -------
        None
        """
        self.handling.cap.release()
        self.button_close.setVisible(False)
        self.button_select.setVisible(False)
        self.button_stop.setVisible(False)

    def on_select_clicked(self):
        """
        Select new SiamMask object, preceeded by proper QMessageBox.

        Returns
        -------
        None
        """
        QMessageBox.information(self, "SiamMask", "Wybierz obiekt i nacisnij spację lub enter.")
        self.handling.new_select()

    def processRCNNImage(self):
        """
        Process image when using MaskRCNN method.

        Returns
        -------
        None
        """
        self.processedImageLabel.setText("Trwa przetwarzanie nagrania")
        self.processedImageLabel.repaint()
        rcnn = MaskRCNN()
        img = cv2.imread(self.filePath, 1)
        im, masks, elapsedTime = rcnn.get_masked_frame(img)
        new_path = 'bufferImage.png'
        cv2.imwrite(new_path, im)
        self.setNewProcessedImage(new_path)
        QMessageBox.information(self, " ", "Zakończono RCNN \nw czasie {} s".format(str(round(elapsedTime, 2))))

    def processSIAMImage(self):  # still need to do somethign here
        """
        Process image when using SiamMask method.

        Returns
        -------
        None
        """
        self.processedImageLabel.setText("Trwa przetwarzanie zdjęcia")
        self.processedImageLabel.repaint()
        davispthPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'SiamMask', 'experiments',
                                    'siammask_sharp', 'SiamMask_DAVIS.pth')
        davisjsonPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'SiamMask', 'experiments',
                                     'siammask_sharp', 'tools', 'utils', 'config_davis.json')
        # runSiamFromGui(self.filePath, True, davispthPath, davisjsonPath)
        list_of_args = []  # lista z argumentami, zmieniana w GUI
        args_class = Args(self.filePath, True, davispthPath, davisjsonPath)
        if self.filePath == '':
            list_of_args.append('--camera')
        args = args_class.parse_args(list_of_args)

        # Parse Image file
        if not args.camera:
            path = args.video
        else:
            path = ''

        self.handling = SiamHandling(path, args)
        siammask, cfg, device = self.handling.setup_model()

        if args.one_frame:
            self.handling.process_one_frame()
        else:
            self.handling.initialize_and_run()

    def processICNETImage(self):
        """
        Process image when using ICNET method.

        Returns
        -------
        None
        """
        self.processedImageLabel.setText("Trwa przetwarzanie zdjęcia")
        self.processedImageLabel.repaint()
        modelPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ICNet_tensorflow_master', 'model',
                                 'cityscapes', 'icnet_cityscapes_trainval_90k.npy')
        createICNETInstance(modelPath, self.filePath, False)

    def processSIAMVideo(self):
        """
        Process video when using SiamMask method.

        Returns
        -------
        None
        """
        self.button_close.setVisible(True)
        self.button_select.setVisible(True)
        if self.ifcam.isChecked():
            self.processedImageLabel.setText("Obraz z kamerki")
            self.button_stop.setVisible(False)
        else:
            self.processedImageLabel.setText("Trwa przetwarzanie filmu")
            self.button_stop.setVisible(True)
        self.processedImageLabel.repaint()
        davispthPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'SiamMask', 'experiments',
                                    'siammask_sharp', 'SiamMask_DAVIS.pth')
        davisjsonPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'SiamMask', 'experiments',
                                     'siammask_sharp', 'tools', 'utils', 'config_davis.json')
        # runSiamFromGui(self.filePath, False, davispthPath, davisjsonPath)
        list_of_args = []  # lista z argumentami, zmieniana w GUI
        args_class = Args(self.filePath, False, davispthPath, davisjsonPath)
        if self.filePath == '':
            list_of_args.append('--camera')
        args = args_class.parse_args(list_of_args)

        # Parse Image file
        if not args.camera:
            path = args.video
        else:
            path = ''

        self.handling = SiamHandling(path, args)
        siammask, cfg, device = self.handling.setup_model()

        if args.one_frame:
            self.handling.process_one_frame()
        else:
            self.handling.initialize_and_run()

    def processICNETVideo(self):
        """
        Process image when using ICNET method.

        Returns
        -------
        None
        """
        def histogram(image):
            color = []
            for channel in cv2.split(image):
                array = np.array(channel).flatten()
                unique, counts = np.unique(array, return_counts=True)
                array_h = np.zeros(256)
                array_h[unique] = counts
                color.append(np.argmax(array_h))
            return tuple(color)

        self.processedImageLabel.setText("Trwa przetwarzanie filmu")
        self.processedImageLabel.repaint()
        modelPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ICNet_tensorflow_master', 'model',
                                 'cityscapes', 'icnet_cityscapes_trainval_90k.npy')
        cap = cv2.VideoCapture(self.filePath)
        ret, frame = cap.read()
        out, masks = createICNETInstance(modelPath, self.filePath, frame)
        X1, Y1, w, h = cv2.selectROI('maska', out, fromCenter=False, showCrosshair=True)
        X2, Y2 = X1 + w, Y1 + h
        selected_mask = masks[Y1:Y2, X1:X2]
        hist = histogram(selected_mask)
        hist = np.asarray(hist)
        color_extracted_mask = cv2.inRange(selected_mask, hist, hist)
        height, width, _ = out.shape
        masks = np.zeros((height, width))
        masks[Y1:Y2, X1:X2] = color_extracted_mask
        masks = cv2.resize(masks, (frame.shape[1], frame.shape[0]))
        masks = [masks]
        cv2.destroyAllWindows()

        if self.chosenTracker == 'ORB':
            np_masks = np.array(masks, dtype=np.uint8)
            orb(cap, np_masks[0], frame)

        else:
            if self.chosenTracker == 'DLIB':
                tracker = DlibTracker()
                tracker.bounding_box = (X1, Y1, X2, Y2)
                tracker.tracker.start_track(frame, dlib.rectangle(X1, Y1, X2, Y2))
            elif self.chosenTracker == 'CV2':
                tracker = CVTracker()
                tracker.bounding_box = (X1, Y1, X2, Y2)
                tracker.__multiTracker__.add(cv2.TrackerCSRT_create(), frame, tracker.bounding_box)
            while cap.isOpened():
                ret, frame = cap.read()
                if not ret:
                    break

                tracker.start(frame, frame)
                cv2.imshow("Frame", frame)
                key = cv2.waitKey(1) & 0xFF
                if key == ord("q"):
                    break
            cv2.destroyAllWindows()
            cap.release()

    def processRCNNVideo(self):
        """
        Process image when using MaskRCNN method.

        Returns
        -------
        None
        """
        self.processedImageLabel.setText("Trwa przetwarzanie filmu")
        self.processedImageLabel.repaint()

        cap = cv2.VideoCapture(self.filePath)
        ret, frame = cap.read()
        rcnnObj = MaskRCNN()
        im, masks, elapsedTime = rcnnObj.get_masked_frame(frame)

        X1, Y1, w, h = cv2.selectROI('maska', im, fromCenter=False, showCrosshair=True)
        X2, Y2 = X1 + w, Y1 + h

        cv2.destroyAllWindows()

        if self.chosenTracker == 'ORB':
            np_masks = np.array(masks, dtype=np.uint8)
            np_masks = 1 * np_masks
            mask = None
            for i in range(np.shape(masks)[0]):
                if np.any(np_masks[i, Y1:Y2, X1:X2] == 1):
                    mask = np_masks[i]
                    break
            orb(cap, mask, frame)

        else:
            if self.chosenTracker == 'DLIB':
                tracker = DlibTracker()
                tracker.bounding_box = (X1, Y1, X2, Y2)
                tracker.tracker.start_track(frame, dlib.rectangle(X1, Y1, X2, Y2))
            elif self.chosenTracker == 'CV2':
                tracker = CVTracker()
                tracker.bounding_box = (X1, Y1, X2, Y2)
                tracker.__multiTracker__.add(cv2.TrackerCSRT_create(), frame, tracker.bounding_box)
            while cap.isOpened():
                ret, frame = cap.read()
                if not ret:
                    break

                tracker.start(frame, frame)
                cv2.imshow("Frame", frame)
                key = cv2.waitKey(1) & 0xFF
                if key == ord("q"):
                    break
            cv2.destroyAllWindows()
            cap.release()

        # rcnn = MaskRCNN()
        # img = cv2.imread(self.filePath, 1)
        # im, masks, elapsedTime = rcnn.get_masked_frame(img)
        # new_path = 'bufferImage.png'
        # cv2.imwrite(new_path, im)
        # self.setNewProcessedImage(new_path)
        # QMessageBox.information(self, " ", "Zakończono RCNN \nw czasie {} s".format(str(round(elapsedTime, 2))))

    def startProcessing(self):
        """
        Start processing image/video based on chosen elements.

        Returns
        -------
        None
        """
        if not self.filePath:
            if self.chosenSegmentFunc == 'SIAM' and self.ifcam.isChecked():
                self.filePath = ''
            else:
                QMessageBox.information(self, "Problem", "Nie masz wybranego zdjęcia")
                return

        if self.chosenSegmentFunc == 'RCNN':
            if self.imageChosen:
                self.processRCNNImage()
            else:
                self.processRCNNVideo()
        elif self.chosenSegmentFunc == 'SIAM':
            if self.ifcam.isChecked():
                cap = cv2.VideoCapture(0 + cv2.CAP_DSHOW)
                if not cap.read()[0]:
                    QMessageBox.information(self, "Problem", "Nie wykryto kamery")
                    cap.release()
                    return
                cap.release()
            if self.imageChosen:
                self.processSIAMImage()
            else:
                self.processSIAMVideo()
        elif self.chosenSegmentFunc == 'ICNET':
            if self.imageChosen:
                self.processICNETImage()
            else:
                self.processICNETVideo()
        else:
            print('strange behaviour,we shouldnt be here')

    def changeFunc(self, chosen):
        """
        Change/update all necessary elements when segmentation method is chosen from dropdown menu.

        Parameters
        ----------
        chosen : str
            Chosen segmentation method.

        Returns
        -------
        None
        """
        self.chosenSegmentFunc = self.segmentFuncSet[chosen]
        if self.chosenSegmentFunc == 'SIAM':
            self.trackerDropdown.hide()
        else:
            self.trackerDropdown.show()
        self.update()
        print(self.chosenSegmentFunc)

    def changeTracker(self, chosen):
        """
        Change/update all necessary elements when tracker is chosen from dropdown menu.

        Parameters
        ----------
        chosen : str
            Chosen tracker.

        Returns
        -------
        None
        """
        self.chosenTracker = self.trackersSet[chosen]
        print(self.chosenTracker)

    def setNewProcessedImage(self, imgPath):
        """
        Change/update all necessary elements when function is called (show processed image in GUI).

        Parameters
        ----------
        imgPath : str
            Path/name of the image.

        Returns
        -------
        None
        """
        myImage = QPixmap(imgPath)
        self.processedImageLabel.setPixmap(myImage)
        self.processedImageLabel.setScaledContents(True)
        self.update()

    def openFiles(self):
        """
        Handle file selection when `Glowne.pickFileButton` is clicked.

        Returns
        -------
        None
        """
        data_path, _ = QFileDialog.getOpenFileName(None, 'Otwórz plik')
        if data_path:
            myPath = data_path.replace(os.sep, '/')
            self.filePath = myPath
            copied = myPath
            fileEnding = copied.split('.')[1]
            imageEndings = ['jpg', 'png', 'tif', 'gif']
            videoEndings = ['mp4']
            totalArr = imageEndings + videoEndings
            if fileEnding in totalArr:
                self.filePathLabel.setText(str(myPath))
                if fileEnding in imageEndings:
                    self.imageChosen = True
                    self.videoChosen = False
                    myImage = QPixmap(myPath)
                    self.imageLabel.setPixmap(myImage)
                    self.imageLabel.setScaledContents(True)
                    self.processedImageLabel.clear()
                    self.update()
                else:
                    print('wideo')
                    self.imageChosen = False
                    self.videoChosen = True
            else:
                QMessageBox.information(self, "Problem", "Zły format pliku")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    palette = QPalette()
    palette.setColor(QPalette.Button, QtCore.Qt.green)
    app.setPalette(palette)
    sz = app.primaryScreen().size()
    ex = Glowne(sz.width(), sz.height())
    sys.exit(app.exec_())

# Jak wrzucać kod na gitlab

## Ogólne zasady
1. Starajcie się nazywać commity jak najbardziej sensownie, żeby było to w miarę czytelne
2. Do każdego commitu wrzucajcie najlepiej jedną funkcjonalność, żeby w razie konieczności powrotu do jakiegoś punktu 
cofać się o tylko jeden krok
3. Nazywajcie branche najelpiej numerami Issue(np. Issue9)

## Procedura

1. Jesteśmy na branchu master
2. Pobieramy najnowsze zmiany poprzez:
    ```sh
    $ git pull origin
    ```
2. Jeśli chcemy coś dodać/zmienić - tworzymy nowego brancha i przełączamy się na niego komendą:
    ```sh
    $ git checkout -b NAZWA_BRANCHA
    ```
3. Dodajemy/edytujemy pliki projektu
4. Dodajemy pliki do repozytorium, domyślam się że zazwyczaj chcemy dodać wszystkie pliki na których pracowaliśmy(stąd -A)
    ```sh
    $ git add -A
    ```
5. Commitujemy dodane pliki
    ```sh
    $ git commit -m 'NAZWA COMMITU'
    ```
6. Przechodzimy na branch master
    ```sh
    $ git checkout master
    ```
7. Pobieramy ewentualne najnowsze zmiany z repozytorium zdalnego
    ```sh
    $ git pull origin
    ```
8. Przechodzimy na nasz branch i robimy rebase
    ```sh
    $ git checkout NAZWA_BRANCHA
    $ git rebase master
    ```
9. Pushujemy pliki
    ```sh
    $ git push origin NAZWA_BRANCHA
    ```
10. Wyświetli nam się w konsoli link w który powinniśmy wejść przez przeglądarke
![alt text](img.JPG)
11. W polu description dobrze opisać co dodajemy w danym merge requescie
12. W polu assignee wybieramy osobe z projektu która ma ocenić/zrecenzować nasz kod
13. Dajemy submit merge request
14. Dobrze by było jakbyście wtedy na konwersacji wysłali link do merge requesta i oznaczyli osobe do sprawdzenia.
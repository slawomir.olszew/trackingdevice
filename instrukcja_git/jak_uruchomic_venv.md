# Jak uruchamiać Virtual Environment

## Wiersz polecenia/Powershell

1. Uruchamiamy konsolę w głównym folderze projektu, tam gdzie znajduje się folder venv.
2. Wpisujemy polecenie:
    Wiersz polecenia
    ```sh
    $ venv\Scripts\activate.bat
    ```
    Powershell
    ```sh
    $ venv\Scripts\activate.ps1
    ```
	Po wykonaniu powinniśmy zobaczyć napis (venv) po lewej stronie wiersza w konsoli.
![alt text](venv.png)
3. Teraz możemy uruchomić dowolny program, np.:
    ```sh
    $ python .\mask-r-cnn\maskrcnn_example.py
    ```
	
## Pycharm
1. Otwieramy Pycharm na wybranym projekcie i naciskamy jednocześnie CTRL+ALT+S.
2. W prawym górnym rogu wybieramy ikonę ustawień poczym klikamy oraz Add..
![alt text](pycharm.png)
3. Zaznaczamy Existing environment i szukamy scieżki do venv\Scripts\python.exe
![alt text](pycharm2.png)
5. Wybieramy OK oraz Apply, po czym czekamy na zakończenie indexowania
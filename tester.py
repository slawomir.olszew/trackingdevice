import argparse
import os
import sys
import time

import cv2
from datetime import datetime

# Root directory of the project
ROOT_DIR = os.path.abspath("../../")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mask_r_cnn.mask_rcnn import MaskRCNN


class NeuralTester:
    def __init__(self):
        self.mask_r_cnn = MaskRCNN()
        self.ic_net = None
        self.siam_mask = None

        self.neural_types = ["icnet", "mask-r-cnn", "siam-mask"]

        self.current_nn_type = None

        self.SAVE_PATH = os.path.join('test_videos', "output_video")
        self.VIDEO_PATH = os.path.join('test_videos', "input_test.mp4")

    def set_neural_network(self, nn_type):
        nn_type = nn_type.lower()

        if nn_type in self.neural_types:
            self.current_nn_type = nn_type
        else:
            assert "Invalid neural network name."

    def get_masked_frame(self, frame):
        if self.current_nn_type == "icnet":
            return None
        elif self.current_nn_type == "mask-r-cnn":
            return self.mask_r_cnn.get_masked_frame(frame)
        elif self.current_nn_type == "siammask":
            return None
        else:
            assert "Neural network name not set."

    def save_video(self, frames, fps):
        height, width, layers = frames[-1].shape

        now = datetime.now()
        timestamp = str(now.strftime("%Y%m%d_%H-%M-%S")) + ".mp4"

        if fps == 0:
            fps = 15

        video = cv2.VideoWriter(self.SAVE_PATH + timestamp, -1, fps, (width, height))

        for frame in frames:
            video.write(frame)

        video.release()

    def run_test(self, mode, save):
        print("Press 'q' to stop the test.")
        loop_end = False
        frames_array = []
        if mode == 'camera':
            cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        else:
            cap = cv2.VideoCapture(self.VIDEO_PATH)
        while not loop_end:
            ret, frame = cap.read()
            t_before = time.perf_counter()
            im, masks = self.get_masked_frame(frame)
            t_after = time.perf_counter()
            cv2.imshow("masked_img", im)
            frames_array.append(im)
            key = cv2.waitKey(1) & 0xFF

            if key == ord('q'):
                loop_end = True
                if save == 'save':
                    fps = 1 / (t_after - t_before)
                    self.save_video(frames_array, fps)
                cv2.destroyAllWindows()


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-type', '--TYPE', default='mask-r-cnn', help='Neural network type.')
    ap.add_argument('-m', '--MODE', default='video', help='Choose video or camera')
    ap.add_argument('-s', '--SAVE', default='save', help='Choose video or camera')
    args = vars(ap.parse_args())
    tester = NeuralTester()
    tester.set_neural_network(args['TYPE'])
    tester.run_test(args['MODE'], args['SAVE'])

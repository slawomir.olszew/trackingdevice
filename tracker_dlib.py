"""
### Tracker module using [tracker from dlib library](http://dlib.net/python/index.html#dlib.correlation_tracker). Can be used with [MaskRCNN](mask_r_cnn.rcnn.html) and [ICNet](ICNet_tensorflow_master.ICNet.html)
"""
import dlib
import cv2


class Tracker:
    """
    ## Dlib tracker class.

    Attributes
    ----------
    bounding_box : tuple
        Tuple that contains bounding box coordinates (x1, y1, x2, y2).
    tracker : dlib.correlation_tracker
        Dlib tracker object.
    """
    def __init__(self):
        self.bounding_box = None
        self.tracker = dlib.correlation_tracker()

    def __tracker__(self, frame):
        """
        Update tracker and bounding box coordinates based on given frame.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        
        Returns
        -------
        None
        """
        self.tracker.update(frame)
        rect = self.tracker.get_position()
        x1, y1 = (int(rect.left()), int(rect.top()))
        x2, y2 = (int(rect.right()), int(rect.bottom()))
        self.bounding_box = (x1, y1, x2, y2)

    def start(self, frame, output):
        """
        Main tracker function, called every read frame. Draw bounding box and arrows (if object is near edges) on frame.

        Parameters
        ----------
        frame : numpy.ndarray
            cv2 frame.
        output: numpy.ndarray
            cv2 output frame.
        
        Returns
        -------
        None
        """
        if self.bounding_box is not None:
            self.__tracker__(frame)
            x1, y1, x2, y2 = self.bounding_box
            cv2.rectangle(output, (x1, y1), (x2, y2), (0, 255, 0), 2)

            if (x2 + 150) > output.shape[1]:
                # obiekt zbyt nisko i w prawo
                if (y2 + 150) > output.shape[0]:
                    cv2.arrowedLine(output, (750, 225), (750, 100), (0, 255, 0), 5)     # zbyt nisko
                    cv2.arrowedLine(output, (750, 225), (625, 225), (0, 255, 0), 5)     # zbyt w prawo
                # obiekt zbyt wysoko i w prawo
                elif (y1 - 150) < 0:
                    cv2.arrowedLine(output, (750, 100), (750, 225), (0, 255, 0), 5)     # zbyt wysoko
                    cv2.arrowedLine(output, (750, 100), (625, 100), (0, 255, 0), 5)     # zbyt w prawo
                # obiekt zbyt w prawo
                else:
                    cv2.arrowedLine(output, (750, 100), (625, 100), (0, 255, 0), 5)

            elif (x1 - 150) < 0:
                # obiekt zbyt nisko i w lewo
                if (y2 + 150) > output.shape[0]:
                    cv2.arrowedLine(output, (625, 225), (625, 100), (0, 255, 0), 5)     # zbyt nisko
                    cv2.arrowedLine(output, (625, 225), (750, 225), (0, 255, 0), 5)     # zbyt w lewo
                # obiekt zbyt wysoko i w lewo
                elif (y1 - 150) < 0:
                    cv2.arrowedLine(output, (625, 100), (625, 225), (0, 255, 0), 5)     # zbyt wysoko
                    cv2.arrowedLine(output, (625, 100), (750, 100), (0, 255, 0), 5)     # zbyt w lewo
                # obiekt zbyt w lewo
                else:
                    cv2.arrowedLine(output, (625, 100), (750, 100), (0, 255, 0), 5)

            # obiekt zbyt nisko
            elif (y2 + 150) > output.shape[0]:
                cv2.arrowedLine(output, (690, 225), (690, 100), (0, 255, 0), 5)
            # obiekt zbyt wysoko
            elif (y1 - 150) < 0:
                cv2.arrowedLine(output, (690, 100), (690, 225), (0, 255, 0), 5)

    def bbox_choice(self, output):
        """
        Choose bounding box using `cv2.selectROI()` and start tracking. Not used.

        Parameters
        ----------
        output: numpy.ndarray
            cv2 output frame.
        
        Returns
        -------
        None
        """
        x, y, w, h = cv2.selectROI("Frame", output, fromCenter=False, showCrosshair=True)
        self.bounding_box = (x, y, x + w, y + h)
        self.tracker.start_track(output, dlib.rectangle(x, y, x + w, y + h))

    def new_bbox(self, output):
        """
        Choose new bounding box using `cv2.selectROI()` and start tracking. Reset bounding_box attribute first. Not used.

        Parameters
        ----------
        output: numpy.ndarray
            cv2 output frame.
        
        Returns
        -------
        None
        """
        self.bounding_box = None
        x, y, w, h = cv2.selectROI("Frame", output, fromCenter=False, showCrosshair=True)
        self.bounding_box = (x, y, x + w, y + h)
        self.tracker.start_track(output, dlib.rectangle(x, y, x + w, y + h))

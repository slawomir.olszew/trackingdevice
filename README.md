# TrackingDevice
Opracowanie metody śledzenia (dowolnych - klikniętych) obiektów na strumieniu z kamery (segmentacja semantyczna, 
a następnie śledzenie).
## Założenia projektowe
1. Celem proejktu jest stworzenie aplikacji desktopowej w języku Python, która będzie umożliwiać segmentację obrazu oraz śledzenie wybranego obiektu 
po segmentacji.
2. Jako źródła obrazu będziemy używać kamerę internetową lub filmiki, na których segmentacja będzie najlepiej widoczna. 
3. Sieci jakie chcemy wykorzystać do segmentacji:
    
    a) Mask R-CNN (https://github.com/matterport/Mask_RCNN)
    
    b) ICNet (https://github.com/hellochick/ICNet-tensorflow)
    
    c) SiamMask (https://github.com/foolwood/SiamMask)
4. Metody śledzenia jakie chcemy wykorzystać:
    
    a) optical flow + orb descriptor
    
    b) trackery z biblioteki Dlib
    
    c) trackery z biblioteki OpenCV
    
5. Porównywać będziemy jedynie trackery, oceniać je będziemy na podstawie dokładności śledzenia, zużytych zasobów oraz 
czasu obliczeń. 
6. Aplikacja będzie posiadała GUI (PyQT),w którym poza wyświetlanymi klatkami obrazu będzie można wybrać metodę segmetnacji 
oraz algorytm śledzenia obiektu.
7. Będzie możliwa zmiana obiektu w trakcie śledzenia. 
8. Kod będzie na bieżaco komentowany, finalnie chcemy wygenerować dokumentację przy użyciu bibilioteki PyDoc.
 Do dokumentacji dodamy również krótki opis dotyczący zastosowanych w projekcie rozwiązań.
 
## Raport porównawczy
### Metody segmentacji 
 Cecha | **MaskRCNN** | **ICNet** | **SiamMask**
 --- | ---- | --- | ---
 **Dokladność sieci(%)** | 65.632 | 80.9 | 71.3
  **FPS przy segmentacji live** | 0.419 | 0.992 | 100.5 - jeden obiekt<br> 42.1 - dwa obiekty
  **Ilosc klas obiektow** | 81 | 19 | 1000
  **Architektura** | FPN + ResNet101 | ResNet50 z 3 pomocniczymi gałęziami | Siamese
  **Dataset** | MS COCO | Cityscapes | ImageNet-1k
  
 Podsumowując, najlepszym wyborem jest metoda SiamMask. Pozwala ona na przetwarzanie obrazu live
 bez znacznego wpływu na płynność, posiada największą ilość rozpoznawanych klas, możliwe jest śledzenie
 wielu obiektów jednocześnie.

### Metody śledzenia (trackery) 
Cecha | **Optical Flow + Orb Descriptor** | **OpenCV (CSRT Tracker)** | **Dlib Tracker**
--- | --- | ---| ---
**Ilość FPS (średnia)** | 19.86 | 2.25 | 110.3
**Źródło śledzenia** | Maska | Bounding box | Bounding box

Z wyników oraz obserwacji samego działania można wywnioskować, że najlepszym trackerem (metodą śledzenia) jest
ten z biblioteki Dlib. Cechuje się on dużą szybkością działania i samo śledzenie również jest dokładne (bouding box jest zawsze w obrębie śledzonego obiektu i nie "lata" po ekranie). Wybrany tracker z biblioteki OpenCV jest niestety zbyt wolny dla wygodnego
użytkowania. Metoda łącząca Optical Flow oraz Orb Descriptor, mimo pożądanego działania (wykorzystanie maski z segmentacji semantycznej) oraz akceptowalnej szybkości działania, nie jest jednak tak dokładny, jakby tego oczekiwano (poprawę możnaby uzyskać doborem innych parametrów i/lub ciągłą segmentacją (aktualna maska), co jednak mogłoby mieć dość negatywny wpływ na szybkość działania).
Poza opisanymi i zaimplementowanymi trackerami jest również jeden wbudowany w metodę segmentacji SiamMask, którego szybkość można zakwalifikować do szybkości samej segmentacja, która jest zadowalająca oraz bardzo dokładna.

## Uruchamianie programu
W celu poprawnego uruchomienia naszego programu konieczne jest skorzystanie z wirtualnego środowiska znajdującego się w naszym repozytorium. W tym celu potrzebny będzie menadżer pakietów conda.
    ```
    $ conda activate ./env
    ```

Koniecznym może się okazać podanie pełnej scieżki do środowiska.
Po poprawnej aktywacji środowiska uruchamiamy program komendą:
    ```
    $ python gui.py
    ```
